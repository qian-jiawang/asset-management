-- MySQL dump 10.13  Distrib 8.0.29, for macos12.2 (x86_64)
--
-- Host: localhost    Database: asset_management
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb3 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_act_log`
--

DROP TABLE IF EXISTS `admin_act_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_act_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL COMMENT '关联的用户ID',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `content` varchar(100) DEFAULT NULL COMMENT '记录',
  `status` int DEFAULT '1' COMMENT '状态',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci COMMENT='操作日志记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_act_log`
--

LOCK TABLES `admin_act_log` WRITE;
/*!40000 ALTER TABLE `admin_act_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_act_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_dept`
--

DROP TABLE IF EXISTS `admin_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_dept` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `parent_id` int DEFAULT NULL COMMENT '父级编号',
  `dept_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '部门名称',
  `sort` int DEFAULT NULL COMMENT '排序',
  `leader` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '负责人',
  `phone` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '联系方式',
  `email` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `enable` int DEFAULT NULL COMMENT '状态(1开启,0关闭)',
  `remark` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci COMMENT '备注',
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '详细地址',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_dept`
--

LOCK TABLES `admin_dept` WRITE;
/*!40000 ALTER TABLE `admin_dept` DISABLE KEYS */;
INSERT INTO `admin_dept` VALUES (1,0,'总公司',1,'总裁办','12312345679','123qq.com',1,NULL,'这是总公司',NULL,'2022-06-01 17:23:20'),(4,1,'济南分公司',2,'分公司管理员01','12312345678','1234qq.com',1,NULL,'这是济南','2022-06-01 17:24:33','2022-06-01 17:25:19'),(5,1,'唐山分公司',4,'分公司管理员02','12312345678','123@qq.com',1,NULL,'这是唐山','2022-06-01 17:25:15','2022-06-01 17:25:20'),(7,4,'济南分公司开发部',5,'分公司经理01','12312345678','123@qq.com',1,NULL,'测试','2022-06-01 17:27:39','2022-06-01 17:27:39'),(8,5,'唐山测试部',6,'分公司经理02','12312345678','123@qq.com',1,NULL,'测试部','2022-06-01 17:28:27','2022-06-01 17:28:27');
/*!40000 ALTER TABLE `admin_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_dict_data`
--

DROP TABLE IF EXISTS `admin_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_dict_data` (
  `id` int NOT NULL AUTO_INCREMENT,
  `data_label` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字典类型名称',
  `data_value` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字典类型标识',
  `type_code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字典类型描述',
  `is_default` int DEFAULT NULL COMMENT '是否默认',
  `enable` int DEFAULT NULL COMMENT '是否开启',
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_dict_data`
--

LOCK TABLES `admin_dict_data` WRITE;
/*!40000 ALTER TABLE `admin_dict_data` DISABLE KEYS */;
INSERT INTO `admin_dict_data` VALUES (8,'男','boy','user_sex',NULL,1,'男 : body','2022-04-16 13:36:34','2022-04-16 14:05:06'),(9,'女','girl','user_sex',NULL,1,'女 : girl','2022-04-16 13:36:55','2022-04-16 13:36:55');
/*!40000 ALTER TABLE `admin_dict_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_dict_type`
--

DROP TABLE IF EXISTS `admin_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_dict_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字典类型名称',
  `type_code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字典类型标识',
  `description` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '字典类型描述',
  `enable` int DEFAULT NULL COMMENT '是否开启',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_dict_type`
--

LOCK TABLES `admin_dict_type` WRITE;
/*!40000 ALTER TABLE `admin_dict_type` DISABLE KEYS */;
INSERT INTO `admin_dict_type` VALUES (1,'用户性别','user_sex','用户性别',1,NULL,'2022-04-16 13:37:11');
/*!40000 ALTER TABLE `admin_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_log`
--

DROP TABLE IF EXISTS `admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `method` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` int DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `ip` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `create_time` datetime DEFAULT NULL,
  `success` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1585 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_log`
--

LOCK TABLES `admin_log` WRITE;
/*!40000 ALTER TABLE `admin_log` DISABLE KEYS */;
INSERT INTO `admin_log` VALUES (1485,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-18 18:39:36',1),(1486,'PUT',1,'/admin/power/update','{\"powerId\":\"52\",\"parentId\":\"0\",\"powerName\":\"定时任务\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-log\",\"sort\":\"900\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-19 17:02:37',1),(1487,'PUT',1,'/admin/power/update','{\"powerId\":\"17\",\"parentId\":\"0\",\"powerName\":\"文件管理\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-camera\",\"sort\":\"600\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-19 17:02:46',1),(1488,'PUT',1,'/admin/power/update','{\"powerId\":\"1\",\"parentId\":\"0\",\"powerName\":\"系统管理\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-set-fill\",\"sort\":\"400\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-19 17:02:53',1),(1489,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"采购管理\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon-gift\",\"sort\":\"300\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-19 17:04:09',1),(1490,'POST',1,'/admin/power/save','{\"parentId\":\"1\",\"powerName\":\"供应商管理\",\"selectParent_select_input\":\"系统管理\",\"powerCode\":\"suppliers\",\"powerType\":\"1\",\"powerUrl\":\"admin/suppliers/main\",\"openType\":\"_blank\",\"icon\":\"layui-icon layui-icon-light\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 08:36:54',1),(1491,'PUT',1,'/admin/power/update','{\"powerId\":\"58\",\"parentId\":\"1\",\"powerName\":\"供应商管理\",\"selectParent_select_input\":\"系统管理\",\"powerCode\":\"suppliers\",\"powerType\":\"1\",\"powerUrl\":\"admin/supplier/main\",\"openType\":\"_blank\",\"icon\":\"layui-icon layui-icon layui-icon-light\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 08:38:04',1),(1492,'PUT',1,'/admin/power/update','{\"powerId\":\"58\",\"parentId\":\"1\",\"powerName\":\"供应商管理\",\"selectParent_select_input\":\"系统管理\",\"powerCode\":\"suppliers\",\"powerType\":\"1\",\"powerUrl\":\"/admin/supplier/main\",\"openType\":\"_blank\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon-light\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 08:38:32',1),(1493,'PUT',1,'/admin/power/update','{\"powerId\":\"58\",\"parentId\":\"1\",\"powerName\":\"供应商管理\",\"selectParent_select_input\":\"系统管理\",\"powerCode\":\"admin:supplier:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/supplier\",\"openType\":\"_blank\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon-light\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 08:39:21',1),(1494,'PUT',1,'/admin/power/update','{\"powerId\":\"58\",\"parentId\":\"1\",\"powerName\":\"供应商管理\",\"selectParent_select_input\":\"系统管理\",\"powerCode\":\"admin:supplier:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/supplier/main\",\"openType\":\"_blank\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-light\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 08:40:19',1),(1495,'PUT',1,'/admin/power/update','{\"powerId\":\"58\",\"parentId\":\"1\",\"powerName\":\"供应商管理\",\"selectParent_select_input\":\"系统管理\",\"powerCode\":\"admin:supplier:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/supplier\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-light\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 08:41:01',1),(1496,'PUT',1,'/admin/power/update','{\"powerId\":\"58\",\"parentId\":\"1\",\"powerName\":\"供应商管理\",\"selectParent_select_input\":\"系统管理\",\"powerCode\":\"admin:supplier:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/supplier/main\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-light\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 08:42:30',1),(1497,'POST',1,'/admin/supplier/enable','{\"id\":\"1\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 09:33:01',1),(1498,'POST',1,'/admin/supplier/disable','{\"id\":\"1\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 09:33:52',1),(1499,'POST',1,'/admin/supplier/enable','{\"id\":\"1\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 09:33:54',1),(1500,'POST',1,'/admin/power/save','{\"parentId\":\"57\",\"powerName\":\"设置\",\"selectParent_select_input\":\"采购管理\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon \",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 10:52:03',1),(1501,'POST',1,'/admin/power/save','{\"parentId\":\"59\",\"powerName\":\"资产/物品分类管理\",\"selectParent_select_input\":\"设置\",\"powerCode\":\"\",\"powerType\":\"1\",\"powerUrl\":\"/admin/category\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 10:52:38',1),(1502,'POST',1,'/admin/power/save','{\"parentId\":\"59\",\"powerName\":\"资产/物品分类管理\",\"selectParent_select_input\":\"设置\",\"powerCode\":\"admin:category:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/category\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 10:52:50',1),(1503,'POST',1,'/admin/power/save','{\"parentId\":\"59\",\"powerName\":\"资产/物品分类管理\",\"selectParent_select_input\":\"设置\",\"powerCode\":\"admin:category:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/category\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 10:52:58',1),(1504,'POST',1,'/admin/power/save','{\"parentId\":\"59\",\"powerName\":\"通用设置\",\"selectParent_select_input\":\"设置\",\"powerCode\":\"admin:category:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/category\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 10:53:38',1),(1505,'POST',1,'/admin/power/save','{\"parentId\":\"57\",\"powerName\":\"采购申请\",\"selectParent_select_input\":\"采购管理\",\"powerCode\":\"admin:purchasing:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/purchasing/\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 10:54:42',1),(1506,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"资产管理\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon-gift\",\"sort\":\"20\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:37:04',1),(1507,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"资产列表\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"admin:asset:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:37:39',1),(1508,'PUT',1,'/admin/power/update','{\"powerId\":\"64\",\"parentId\":\"63\",\"powerName\":\"资产列表\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon \",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:37:54',1),(1509,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"资产入库\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/warehousing\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"20\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:39:03',1),(1510,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"领用&退库\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/return\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"30\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:40:20',1),(1511,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"借用&归还\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/borrow\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"40\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:41:18',1),(1512,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"资产调拨\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:allocation\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/allocation\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"50\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:42:29',1),(1513,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"实物信息变更\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/physical\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"60\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:43:35',1),(1514,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"附属资产变更\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/subsidiary\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"100\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:48:29',1),(1515,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"维保信息登记\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/maintenance\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"110\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 11:49:45',1),(1516,'PUT',1,'/admin/power/update','{\"powerId\":\"64\",\"parentId\":\"63\",\"powerName\":\"资产列表\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/main\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon layui-icon \",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 13:13:31',1),(1517,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"清理报废\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:clean\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/clean\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"120\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:07:37',1),(1518,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"盘点管理\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:pandian\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/pandian\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:08:14',1),(1519,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"盘点管理\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:pandian\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/pandian\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"130\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:08:21',1),(1520,'POST',1,'/admin/power/save','{\"parentId\":\"63\",\"powerName\":\"分析报表\",\"selectParent_select_input\":\"资产管理\",\"powerCode\":\"admin:asset:report\",\"powerType\":\"1\",\"powerUrl\":\"/admin/asset/report\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"140\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:08:53',1),(1521,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"设备管理\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"admin:device:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/device/main\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"5\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:09:35',1),(1522,'PUT',1,'/admin/power/update','{\"powerId\":\"75\",\"parentId\":\"0\",\"powerName\":\"设备管理\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon \",\"sort\":\"5\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:09:51',1),(1523,'POST',1,'/admin/power/save','{\"parentId\":\"75\",\"powerName\":\"首页\",\"selectParent_select_input\":\"设备管理\",\"powerCode\":\"admin:device:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin/device/main\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"5\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:10:20',1),(1524,'POST',1,'/admin/power/save','{\"parentId\":\"75\",\"powerName\":\"设备列表\",\"selectParent_select_input\":\"设备管理\",\"powerCode\":\"admin:device:main\",\"powerType\":\"1\",\"powerUrl\":\"/admin:device:list\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:11:13',1),(1525,'POST',1,'/admin/power/save','{\"parentId\":\"75\",\"powerName\":\"设备入库\",\"selectParent_select_input\":\"设备管理\",\"powerCode\":\"admin:device:ruku\",\"powerType\":\"1\",\"powerUrl\":\"/admin/device/ruku\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"15\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:12:03',1),(1526,'POST',1,'/admin/power/save','{\"parentId\":\"75\",\"powerName\":\"附属资产变更\",\"selectParent_select_input\":\"设备管理\",\"powerCode\":\"admin:device:change\",\"powerType\":\"1\",\"powerUrl\":\"/admin/device/change\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"20\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:12:41',1),(1527,'POST',1,'/admin/power/save','{\"parentId\":\"75\",\"powerName\":\"配件出库\",\"selectParent_select_input\":\"设备管理\",\"powerCode\":\"admin:device:outstore\",\"powerType\":\"1\",\"powerUrl\":\"/admin/device/outstore\",\"openType\":\"_iframe\",\"icon\":\"layui-icon \",\"sort\":\"30\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:14:26',1),(1528,'PUT',1,'/admin/power/update','{\"powerId\":\"75\",\"parentId\":\"0\",\"powerName\":\"设备管理\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon-heart\",\"sort\":\"5\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-20 15:33:36',1),(1529,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:28:02',1),(1530,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"待办\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon-gift\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:50:34',1),(1531,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"资产\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon-gift\",\"sort\":\"20\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:50:52',1),(1532,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"设备\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon-gift\",\"sort\":\"30\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:51:15',1),(1533,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"库存\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon-gift\",\"sort\":\"40\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:51:49',1),(1534,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"采购\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon-gift\",\"sort\":\"50\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:52:25',1),(1535,'POST',1,'/admin/power/save','{\"parentId\":\"0\",\"powerName\":\"财务\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon-gift\",\"sort\":\"70\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:52:39',1),(1536,'PUT',1,'/admin/power/update','{\"powerId\":\"63\",\"parentId\":\"82\",\"powerName\":\"资产管理\",\"selectParent_select_input\":\"资产\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-gift\",\"sort\":\"20\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:54:06',1),(1537,'PUT',1,'/admin/power/update','{\"powerId\":\"63\",\"parentId\":\"0\",\"powerName\":\"资产管理\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"20\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:54:51',1),(1538,'POST',1,'/admin/power/disable','{\"powerId\":\"82\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:54:58',1),(1539,'PUT',1,'/admin/power/update','{\"powerId\":\"63\",\"parentId\":\"0\",\"powerName\":\"资产\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"20\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 22:55:05',1),(1540,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:12:28',1),(1541,'PUT',1,'/admin/power/update','{\"powerId\":\"1\",\"parentId\":\"0\",\"powerName\":\"系统\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon-set-fill\",\"sort\":\"400\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:16:13',1),(1542,'POST',1,'/admin/power/remove','powerId=82','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:16:31',1),(1543,'PUT',1,'/admin/power/update','{\"powerId\":\"57\",\"parentId\":\"0\",\"powerName\":\"采购\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-gift\",\"sort\":\"300\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:16:44',1),(1544,'POST',1,'/admin/power/remove','powerId=85','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:16:53',1),(1545,'PUT',1,'/admin/power/update','{\"powerId\":\"75\",\"parentId\":\"0\",\"powerName\":\"设备\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-heart\",\"sort\":\"5\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:17:01',1),(1546,'PUT',1,'/admin/power/update','{\"powerId\":\"17\",\"parentId\":\"1\",\"powerName\":\"文件管理\",\"selectParent_select_input\":\"系统\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon-camera\",\"sort\":\"600\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:17:14',1),(1547,'PUT',1,'/admin/power/update','{\"powerId\":\"52\",\"parentId\":\"1\",\"powerName\":\"定时任务\",\"selectParent_select_input\":\"系统\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon-log\",\"sort\":\"900\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:17:23',1),(1548,'POST',1,'/admin/power/remove','powerId=85','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:17:36',1),(1549,'PUT',1,'/admin/power/update','{\"powerId\":\"81\",\"parentId\":\"0\",\"powerName\":\"待办\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-gift\",\"sort\":\"2\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:19:54',1),(1550,'PUT',1,'/admin/power/update','{\"powerId\":\"75\",\"parentId\":\"0\",\"powerName\":\"设备\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon-heart\",\"sort\":\"15\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:20:06',1),(1551,'PUT',1,'/admin/power/update','{\"powerId\":\"82\",\"parentId\":\"0\",\"powerName\":\"资产\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-gift\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:20:25',1),(1552,'POST',1,'/admin/power/remove','powerId=82','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:20:34',1),(1553,'PUT',1,'/admin/power/update','{\"powerId\":\"63\",\"parentId\":\"0\",\"powerName\":\"资产\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"10\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:20:42',1),(1554,'POST',1,'/admin/power/remove','powerId=82','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:20:58',1),(1555,'POST',1,'/admin/power/remove','powerId=82','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:21:15',1),(1556,'POST',1,'/admin/power/remove','powerId=82','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:22:13',1),(1557,'POST',1,'/admin/power/remove','powerId=82','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:22:51',1),(1558,'POST',1,'/admin/power/remove','powerId=82','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:23:22',1),(1559,'POST',1,'/admin/power/remove','powerId=82','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:25:24',1),(1560,'POST',1,'/admin/power/remove','{\"powerId\":82}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:27:07',1),(1561,'PUT',1,'/admin/power/update','{\"powerId\":\"83\",\"parentId\":\"0\",\"powerName\":\"设备\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-gift\",\"sort\":\"300\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:28:09',1),(1562,'PUT',1,'/admin/power/update','{\"powerId\":\"84\",\"parentId\":\"0\",\"powerName\":\"库存\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-gift\",\"sort\":\"400\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:28:17',1),(1563,'PUT',1,'/admin/power/update','{\"powerId\":\"85\",\"parentId\":\"0\",\"powerName\":\"采购\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-gift\",\"sort\":\"500\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:28:23',1),(1564,'PUT',1,'/admin/power/update','{\"powerId\":\"86\",\"parentId\":\"0\",\"powerName\":\"财务\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon-gift\",\"sort\":\"700\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:28:42',1),(1565,'PUT',1,'/admin/power/update','{\"powerId\":\"84\",\"parentId\":\"0\",\"powerName\":\"库存\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"0\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"200\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:29:24',1),(1566,'PUT',1,'/admin/power/update','{\"powerId\":\"84\",\"parentId\":\"81\",\"powerName\":\"待审批任务\",\"selectParent_select_input\":\"待办\",\"powerCode\":\"\",\"powerType\":\"2\",\"powerUrl\":\"\",\"openType\":\"\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"200\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:41:12',1),(1567,'PUT',1,'/admin/power/update','{\"powerId\":\"83\",\"parentId\":\"0\",\"powerName\":\"待签字任务\",\"selectParent_select_input\":\"顶级权限\",\"powerCode\":\"\",\"powerType\":\"1\",\"powerUrl\":\"/admin/main\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"300\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:41:41',1),(1568,'PUT',1,'/admin/power/update','{\"powerId\":\"83\",\"parentId\":\"81\",\"powerName\":\"待签字任务\",\"selectParent_select_input\":\"待办\",\"powerCode\":\"\",\"powerType\":\"1\",\"powerUrl\":\"/admin/main\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"300\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:41:54',1),(1569,'PUT',1,'/admin/power/update','{\"powerId\":\"85\",\"parentId\":\"81\",\"powerName\":\"待处理工单\",\"selectParent_select_input\":\"待办\",\"powerCode\":\"\",\"powerType\":\"1\",\"powerUrl\":\"/admin/main\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"500\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-05-25 23:42:26',1),(1570,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-01 15:24:10',1),(1571,'PUT',1,'/admin/power/update','{\"powerId\":\"84\",\"parentId\":\"81\",\"powerName\":\"待审批任务\",\"selectParent_select_input\":\"待办\",\"powerCode\":\"\",\"powerType\":\"1\",\"powerUrl\":\"/admin/approval/pending\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"200\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-01 15:32:56',1),(1572,'PUT',1,'/admin/power/update','{\"powerId\":\"84\",\"parentId\":\"81\",\"powerName\":\"待审批任务\",\"selectParent_select_input\":\"待办\",\"powerCode\":\"\",\"powerType\":\"1\",\"powerUrl\":\"/admin/approval/pending\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"200\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-01 15:33:33',1),(1573,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 00:37:51',1),(1574,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 00:40:36',1),(1575,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 00:50:42',1),(1576,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 01:02:33',1),(1577,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 02:20:27',1),(1578,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 02:28:10',1),(1579,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 02:28:19',1),(1580,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 02:35:27',1),(1581,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 09:04:40',1),(1582,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 10:22:30',1),(1583,'PUT',1,'/admin/system/power/update','{\"powerId\":\"84\",\"parentId\":\"81\",\"powerName\":\"待审批任务\",\"selectParent_select_input\":\"待办\",\"powerCode\":\"\",\"powerType\":\"1\",\"powerUrl\":\"/admin/approval/pending/main\",\"openType\":\"_iframe\",\"icon\":\"layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-gift\",\"sort\":\"200\"}','::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-02 10:59:37',1),(1584,'POST',1,'/auth/login_post',NULL,'::1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36','2022-06-05 19:41:00',1);
/*!40000 ALTER TABLE `admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_photo`
--

DROP TABLE IF EXISTS `admin_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_photo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `href` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mime` char(50) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `size` char(30) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_photo`
--

LOCK TABLES `admin_photo` WRITE;
/*!40000 ALTER TABLE `admin_photo` DISABLE KEYS */;
INSERT INTO `admin_photo` VALUES (1,'logo.png','/static/admin/admin/images/logo.png','image/png','2204','2022-03-19 18:53:02'),(2,'show.png','/static/admin/admin/images/show.png','image/png','94211','2022-04-01 23:39:41');
/*!40000 ALTER TABLE `admin_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_power`
--

DROP TABLE IF EXISTS `admin_power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_power` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '权限编号',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '权限名称',
  `type` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '权限类型',
  `code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '权限标识',
  `url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '权限路径',
  `open_type` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '打开方式',
  `parent_id` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '父类编号',
  `icon` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图标',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `enable` int DEFAULT NULL COMMENT '是否开启',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_power`
--

LOCK TABLES `admin_power` WRITE;
/*!40000 ALTER TABLE `admin_power` DISABLE KEYS */;
INSERT INTO `admin_power` VALUES (1,'系统','0','','','','0','layui-icon layui-icon layui-icon layui-icon-set-fill',400,NULL,'2022-05-25 23:16:13',1),(3,'用户管理','1','admin:user:main','/admin/system/user/main','_iframe','1','layui-icon layui-icon layui-icon layui-icon layui-icon-rate',1,NULL,NULL,1),(4,'权限管理','1','admin:power:main','/admin/system/power/main','_iframe','1','layui-icon layui-icon-vercode',2,NULL,NULL,1),(9,'角色管理','1','admin:role:main','/admin/system/role/main','_iframe','1','layui-icon layui-icon-username',2,'2021-03-16 22:24:58','2021-03-25 19:15:24',1),(12,'系统监控','1','admin:monitor:main','/admin/system/monitor/main','_iframe','1','layui-icon layui-icon-vercode',5,'2021-03-18 22:05:19','2021-03-25 19:15:27',1),(13,'日志管理','1','admin:log:main','/admin/system/log/main','_iframe','1','layui-icon layui-icon-read',4,'2021-03-18 22:37:10','2022-06-03 11:06:25',1),(17,'文件管理','0','','','','1','layui-icon layui-icon layui-icon layui-icon-camera',600,'2021-03-19 18:56:23','2022-05-25 23:17:14',1),(18,'图片上传','1','admin:file:main','/admin/system/file/main','_iframe','17','layui-icon layui-icon-camera',5,'2021-03-19 18:57:19','2021-03-25 19:15:13',1),(21,'权限增加','2','admin:power:add','','','4','layui-icon layui-icon-add-circle',1,'2021-03-22 19:43:52','2021-03-25 19:15:22',1),(22,'用户增加','2','admin:user:add','','','3','layui-icon layui-icon-add-circle',1,'2021-03-22 19:45:40','2021-03-25 19:15:17',1),(23,'用户编辑','2','admin:user:edit','','','3','layui-icon layui-icon-rate',2,'2021-03-22 19:46:15','2021-03-25 19:15:18',1),(24,'用户删除','2','admin:user:remove','','','3','layui-icon None',3,'2021-03-22 19:46:51','2021-03-25 19:15:18',1),(25,'权限编辑','2','admin:power:edit','','','4','layui-icon layui-icon-edit',2,'2021-03-22 19:47:36','2021-03-25 19:15:22',1),(26,'用户删除','2','admin:power:remove','','','4','layui-icon layui-icon-delete',3,'2021-03-22 19:48:17','2021-03-25 19:15:23',1),(27,'用户增加','2','admin:role:add','','','9','layui-icon layui-icon-add-circle',1,'2021-03-22 19:49:09','2021-03-25 19:15:24',1),(28,'角色编辑','2','admin:role:edit','','','9','layui-icon layui-icon-edit',2,'2021-03-22 19:49:41','2021-03-25 19:15:25',1),(29,'角色删除','2','admin:role:remove','','','9','layui-icon layui-icon-delete',3,'2021-03-22 19:50:15','2021-03-25 19:15:26',1),(30,'角色授权','2','admin:role:power','','','9','layui-icon layui-icon-component',4,'2021-03-22 19:50:54','2021-03-25 19:15:26',1),(31,'图片增加','2','admin:file:add','','','18','layui-icon layui-icon-add-circle',1,'2021-03-22 19:58:05','2021-03-25 19:15:28',1),(32,'图片删除','2','admin:file:delete','','','18','layui-icon layui-icon-delete',2,'2021-03-22 19:58:45','2021-03-25 19:15:29',1),(44,'数据字典','1','admin:dict:main','/admin/system/dict/main','_iframe','1','layui-icon layui-icon-console',6,'2022-04-16 13:59:49','2022-04-16 13:59:49',1),(45,'字典增加','2','admin:dict:add','','','44','layui-icon ',1,'2022-04-16 14:00:59','2022-04-16 14:00:59',1),(46,'字典修改','2','admin:dict:edit','','','44','layui-icon ',2,'2022-04-16 14:01:33','2022-04-16 14:01:33',1),(47,'字典删除','2','admin:dict:remove','','','44','layui-icon ',3,'2022-04-16 14:02:06','2022-04-16 14:02:06',1),(48,'部门管理','1','admin:dept:main','/admin/system/dept/main','_iframe','1','layui-icon layui-icon-group',3,'2022-06-01 16:22:11','2021-07-07 13:49:39',1),(49,'部门增加','2','admin:dept:add','','','48','layui-icon None',1,'2022-06-01 17:35:52','2022-06-01 17:36:15',1),(50,'部门编辑','2','admin:dept:edit','','','48','layui-icon ',2,'2022-06-01 17:36:41','2022-06-01 17:36:41',1),(51,'部门删除','2','admin:dept:remove','','','48','layui-icon None',3,'2022-06-01 17:37:15','2022-06-01 17:37:26',1),(52,'定时任务','0','','','','1','layui-icon layui-icon layui-icon layui-icon-log',900,'2022-06-22 21:09:01','2022-05-25 23:17:23',1),(53,'任务管理','1','admin:task:main','/admin/system/task/main','_iframe','52','layui-icon ',1,'2022-06-22 21:15:00','2022-06-22 21:15:00',1),(54,'任务增加','2','admin:task:add','','','53','layui-icon ',1,'2022-06-22 22:20:54','2022-06-22 22:20:54',1),(55,'任务修改','2','admin:task:edit','','','53','layui-icon ',2,'2022-06-22 22:21:34','2022-06-22 22:21:34',1),(56,'任务删除','2','admin:task:remove','','','53','layui-icon ',3,'2022-06-22 22:22:18','2022-06-22 22:22:18',1),(57,'采购','0','','','','0','layui-icon layui-icon layui-icon-gift',300,'2022-05-19 17:04:09','2022-05-25 23:16:44',1),(58,'供应商管理','1','admin:supplier:main','/admin/system/supplier/main','_iframe','1','layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-light',10,'2022-05-20 08:36:54','2022-05-20 08:42:30',1),(59,'设置','0','','','','57','layui-icon ',10,'2022-05-20 10:52:03','2022-05-20 10:52:03',1),(60,'资产/物品分类管理','1','admin:category:main','/admin/system/category','_iframe','59','layui-icon ',10,'2022-05-20 10:52:58','2022-05-20 10:52:58',1),(61,'通用设置','1','admin:category:main','/admin/system/category','_iframe','59','layui-icon ',10,'2022-05-20 10:53:38','2022-05-20 10:53:38',1),(62,'采购申请','1','admin:purchasing:main','/admin/purchasing/','_iframe','57','layui-icon ',10,'2022-05-20 10:54:42','2022-05-20 10:54:42',1),(63,'资产','0','','','','0','layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-gift',10,'2022-05-20 11:37:04','2022-05-25 23:20:42',1),(64,'资产列表','1','admin:asset:main','/admin/asset/main','_iframe','63','layui-icon layui-icon layui-icon ',10,'2022-05-20 11:37:39','2022-05-20 13:13:31',1),(65,'资产入库','1','admin:asset:main','/admin/asset/warehousing','_iframe','63','layui-icon ',20,'2022-05-20 11:39:03','2022-05-20 11:39:03',1),(66,'领用&amp;退库','1','admin:asset:main','/admin/asset/return','_iframe','63','layui-icon ',30,'2022-05-20 11:40:20','2022-05-20 11:40:20',1),(67,'借用&amp;归还','1','admin:asset:main','/admin/asset/borrow','_iframe','63','layui-icon ',40,'2022-05-20 11:41:18','2022-05-20 11:41:18',1),(68,'资产调拨','1','admin:asset:allocation','/admin/asset/allocation','_iframe','63','layui-icon ',50,'2022-05-20 11:42:29','2022-05-20 11:42:29',1),(69,'实物信息变更','1','admin:asset:main','/admin/asset/physical','_iframe','63','layui-icon ',60,'2022-05-20 11:43:35','2022-05-20 11:43:35',1),(70,'附属资产变更','1','admin:asset:main','/admin/asset/subsidiary','_iframe','63','layui-icon ',100,'2022-05-20 11:48:29','2022-05-20 11:48:29',1),(71,'维保信息登记','1','admin:asset:main','/admin/asset/maintenance','_iframe','63','layui-icon ',110,'2022-05-20 11:49:45','2022-05-20 11:49:45',1),(72,'清理报废','1','admin:asset:clean','/admin/asset/clean','_iframe','63','layui-icon ',120,'2022-05-20 15:07:37','2022-05-20 15:07:37',1),(73,'盘点管理','1','admin:asset:pandian','/admin/asset/pandian','_iframe','63','layui-icon ',130,'2022-05-20 15:08:21','2022-05-20 15:08:21',1),(74,'分析报表','1','admin:asset:report','/admin/asset/report','_iframe','63','layui-icon ',140,'2022-05-20 15:08:53','2022-05-20 15:08:53',1),(75,'设备','0','','','','0','layui-icon layui-icon layui-icon layui-icon-heart',15,'2022-05-20 15:09:35','2022-05-25 23:20:06',1),(76,'首页','1','admin:device:main','/admin/device/main','_iframe','75','layui-icon ',5,'2022-05-20 15:10:20','2022-05-20 15:10:20',1),(77,'设备列表','1','admin:device:main','/admin:device:list','_iframe','75','layui-icon ',10,'2022-05-20 15:11:13','2022-05-20 15:11:13',1),(78,'设备入库','1','admin:device:ruku','/admin/device/ruku','_iframe','75','layui-icon ',15,'2022-05-20 15:12:03','2022-05-20 15:12:03',1),(79,'附属资产变更','1','admin:device:change','/admin/device/change','_iframe','75','layui-icon ',20,'2022-05-20 15:12:41','2022-05-20 15:12:41',1),(80,'配件出库','1','admin:device:outstore','/admin/device/outstore','_iframe','75','layui-icon ',30,'2022-05-20 15:14:26','2022-05-20 15:14:26',1),(81,'待办','0','','','','0','layui-icon layui-icon layui-icon-gift',2,'2022-05-25 22:50:34','2022-05-25 23:19:54',1),(83,'待签字任务','1','','/admin/main','_iframe','81','layui-icon layui-icon layui-icon layui-icon layui-icon-gift',300,'2022-05-25 22:51:15','2022-05-25 23:41:54',1),(84,'待审批任务','1','','/admin/approval/pending/main','_iframe','81','layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-gift',200,'2022-05-25 22:51:49','2022-06-02 10:59:37',1),(85,'待处理工单','1','','/admin/main','_iframe','81','layui-icon layui-icon layui-icon layui-icon-gift',500,'2022-05-25 22:52:25','2022-05-25 23:42:26',1),(86,'财务','0','','','','0','layui-icon layui-icon layui-icon-gift',700,'2022-05-25 22:52:39','2022-05-25 23:28:42',1);
/*!40000 ALTER TABLE `admin_power` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role`
--

DROP TABLE IF EXISTS `admin_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_role` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色名称',
  `code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色标识',
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `details` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '详情',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `enable` int DEFAULT NULL COMMENT '是否启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role`
--

LOCK TABLES `admin_role` WRITE;
/*!40000 ALTER TABLE `admin_role` DISABLE KEYS */;
INSERT INTO `admin_role` VALUES (1,'管理员','admin',NULL,'管理员',1,NULL,NULL,1),(2,'普通用户','common',NULL,'只有查看，没有增删改权限',2,'2022-03-22 20:02:38','2022-04-01 22:29:56',1);
/*!40000 ALTER TABLE `admin_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_power`
--

DROP TABLE IF EXISTS `admin_role_power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_role_power` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '标识',
  `power_id` int DEFAULT NULL COMMENT '用户编号',
  `role_id` int DEFAULT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `power_id` (`power_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  CONSTRAINT `admin_role_power_ibfk_1` FOREIGN KEY (`power_id`) REFERENCES `admin_power` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_role_power_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=367 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_power`
--

LOCK TABLES `admin_role_power` WRITE;
/*!40000 ALTER TABLE `admin_role_power` DISABLE KEYS */;
INSERT INTO `admin_role_power` VALUES (265,1,2),(266,3,2),(267,4,2),(268,9,2),(269,12,2),(270,13,2),(271,17,2),(272,18,2),(273,44,2),(274,48,2),(334,1,1),(335,3,1),(336,4,1),(337,9,1),(338,12,1),(339,13,1),(340,17,1),(341,18,1),(342,21,1),(343,22,1),(344,23,1),(345,24,1),(346,25,1),(347,26,1),(348,27,1),(349,28,1),(350,29,1),(351,30,1),(352,31,1),(353,32,1),(354,44,1),(355,45,1),(356,46,1),(357,47,1),(358,48,1),(359,49,1),(360,50,1),(361,51,1),(362,52,1),(363,53,1),(364,54,1),(365,55,1),(366,56,1);
/*!40000 ALTER TABLE `admin_role_power` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user`
--

DROP TABLE IF EXISTS `admin_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户名',
  `password_hash` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '哈希密码',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `update_at` datetime DEFAULT NULL COMMENT '修改时间',
  `enable` int DEFAULT NULL COMMENT '启用',
  `realname` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '真实名字',
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `avatar` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '头像',
  `dept_id` int DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user`
--

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
INSERT INTO `admin_user` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3',NULL,'2022-06-01 17:28:55',1,'超级管理','要是不能把握时机，就要终身蹭蹬，一事无成！','/static/admin/admin/images/avatar.jpg',1),(7,'test','21232f297a57a5a743894a0e4a801fc3','2021-03-22 20:03:42','2022-06-01 17:29:47',1,'超级管理','要是不能把握时机，就要终身蹭蹬，一事无成','/static/admin/admin/images/avatar.jpg',1),(8,'demo','21232f297a57a5a743894a0e4a801fc3','2022-06-01 17:30:39','2022-06-01 17:30:52',1,'风',NULL,'/static/admin/admin/images/avatar.jpg',7);
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_role`
--

DROP TABLE IF EXISTS `admin_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `admin_user_role` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '标识',
  `user_id` int DEFAULT NULL COMMENT '用户编号',
  `role_id` int DEFAULT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `admin_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user_role`
--

LOCK TABLES `admin_user_role` WRITE;
/*!40000 ALTER TABLE `admin_user_role` DISABLE KEYS */;
INSERT INTO `admin_user_role` VALUES (21,1,1),(22,7,2),(24,8,2);
/*!40000 ALTER TABLE `admin_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `asset` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL COMMENT '资产名称',
  `code` varchar(50) NOT NULL COMMENT '资产编码',
  `rfid` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'RFID',
  `category_id` int DEFAULT NULL COMMENT '资产类别',
  `category_term_id` int DEFAULT NULL COMMENT '标准型号',
  `spec_mode` varchar(50) DEFAULT NULL COMMENT '规格型号',
  `status` int NOT NULL DEFAULT '0' COMMENT '状态',
  `sign_status` int NOT NULL DEFAULT '0' COMMENT '签字状态',
  `unit` varchar(10) DEFAULT NULL COMMENT '计量单位',
  `serial_no` varchar(50) DEFAULT NULL COMMENT '设备序列号',
  `source` varchar(10) DEFAULT NULL COMMENT '来源',
  `amount` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `admin_user_id` int DEFAULT NULL COMMENT '管理员',
  `buy_date` datetime DEFAULT NULL COMMENT '购入日期',
  `assign_user_id` int DEFAULT NULL COMMENT '使用人',
  `service_life` int DEFAULT NULL COMMENT '使用期限(月)',
  `storage_location` varchar(50) DEFAULT NULL COMMENT '存放地点',
  `dept_id` int DEFAULT NULL COMMENT '所属部门',
  `images` varchar(200) NOT NULL COMMENT '照片',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `asset_code_uindex` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT COMMENT='资产信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` VALUES (1,'JD97SN31-RM1-PAZ1','JD97SN31-RM1-PAZ1','',0,0,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'2022-05-19 11:22:34','2022-05-19 11:25:12');
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `category` (
  `id` int NOT NULL COMMENT '主键ID',
  `code` varchar(50) NOT NULL COMMENT '分类编码',
  `name` varchar(50) NOT NULL COMMENT '分类名称',
  `parent_id` int NOT NULL DEFAULT '0' COMMENT '上级分类',
  `service_life` int NOT NULL DEFAULT '36' COMMENT '使用期限(单位月)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci COMMENT='分类表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_term`
--

DROP TABLE IF EXISTS `category_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `category_term` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `category_id` int NOT NULL COMMENT '关联分类ID',
  `name` varchar(50) NOT NULL COMMENT '型号名称',
  `unit` varchar(10) NOT NULL COMMENT '计量单位',
  `status` int NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci COMMENT='分类对应的型号表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_term`
--

LOCK TABLES `category_term` WRITE;
/*!40000 ALTER TABLE `category_term` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_term` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_apply`
--

DROP TABLE IF EXISTS `purchase_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `purchase_apply` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sku_id` int DEFAULT NULL COMMENT '物品id（资产或耗材的原id）',
  `sku_name` varchar(20) DEFAULT NULL COMMENT '物品名称',
  `sku_code` varchar(10) NOT NULL COMMENT '物品编码',
  `sku_unit` varchar(10) DEFAULT NULL COMMENT '物品计量单位',
  `sku_specs` varchar(20) DEFAULT NULL COMMENT '物品规格型号',
  `sku_type` int DEFAULT NULL COMMENT '物品类型（1:资产、2:耗材）',
  `asset_type_code` varchar(20) DEFAULT NULL COMMENT '资产分类编码',
  `asset_name` varchar(20) DEFAULT NULL COMMENT '资产标准名称',
  `weight` int DEFAULT '100' COMMENT '显示的行号（物品的序列号）',
  `quantity` int DEFAULT NULL COMMENT '申请数量',
  `price` int DEFAULT NULL COMMENT '单价',
  `amount` int DEFAULT NULL COMMENT '申请金额',
  `purchased_quantity` int DEFAULT NULL COMMENT '已购买数量',
  `assigned_quantity` int DEFAULT NULL COMMENT '已分配数量',
  `received_quantity` int DEFAULT NULL COMMENT '已收货数量',
  `closed_quantity` int DEFAULT NULL COMMENT '关闭数量',
  `status` int DEFAULT NULL COMMENT '物品状态（1:草稿、2:已提交、3:已审批、4:已驳回、5:已关闭、6:已完成）',
  `application_id` int DEFAULT NULL COMMENT '关联的申请单Id',
  `comment` int DEFAULT NULL COMMENT '物品备注',
  `user_id` int DEFAULT NULL COMMENT '申请人',
  `apply_date` date DEFAULT NULL COMMENT '申请日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `purchase_apply1_skuCode_uindex` (`sku_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci COMMENT='采购申请单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_apply`
--

LOCK TABLES `purchase_apply` WRITE;
/*!40000 ALTER TABLE `purchase_apply` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_apply_asset`
--

DROP TABLE IF EXISTS `purchase_apply_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `purchase_apply_asset` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `purchase_apply_id` int NOT NULL COMMENT '采购申请单ID',
  `apply_num` int NOT NULL COMMENT '申请数量',
  `remark` varchar(200) NOT NULL COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci COMMENT='采购申请明细产品';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_apply_asset`
--

LOCK TABLES `purchase_apply_asset` WRITE;
/*!40000 ALTER TABLE `purchase_apply_asset` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_apply_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sku_goods`
--

DROP TABLE IF EXISTS `sku_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `sku_goods` (
  `id` int NOT NULL AUTO_INCREMENT,
  `skuCode` varchar(20) DEFAULT NULL COMMENT '物品编码',
  `skuType` int NOT NULL COMMENT '物品类型（1：资产、2:耗材）',
  `assetTemplateName` varchar(50) DEFAULT NULL COMMENT '物品名称',
  `assetTypeCode` varchar(20) DEFAULT NULL COMMENT '资产分类编码（如果物品类型为资产，则该项必填）',
  `displayIndex` int NOT NULL DEFAULT '100' COMMENT '显示的行号（物品的序列号）',
  `quantity` int DEFAULT NULL COMMENT '物品数量',
  `amount` int DEFAULT NULL COMMENT '物品单价',
  `price` int DEFAULT NULL COMMENT '物品金额',
  `comment` varchar(50) DEFAULT NULL COMMENT '物品备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci COMMENT='物品详情表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sku_goods`
--

LOCK TABLES `sku_goods` WRITE;
/*!40000 ALTER TABLE `sku_goods` DISABLE KEYS */;
/*!40000 ALTER TABLE `sku_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb3 */;
CREATE TABLE `supplier` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL COMMENT '供应商名称',
  `contact_name` varchar(50) DEFAULT NULL COMMENT '联系人',
  `contact_phone` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `bank_name` varchar(50) NOT NULL COMMENT '付款开户行',
  `bank_account` varchar(50) NOT NULL COMMENT '付款开户账号',
  `remark` varchar(200) NOT NULL COMMENT '备注',
  `enable` int NOT NULL DEFAULT '1' COMMENT '是否启用',
  `status` int NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci COMMENT='供应商信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (1,'华为H3C思科厂商授权店','刘经理','13988881111','上海交通银行XX支行','2002919xxxxx0002','',1,1,'2022-05-19 16:30:27','2022-05-20 09:33:53');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-05 20:35:38
