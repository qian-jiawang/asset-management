-- 菜单SQL
-- delete from admin_power where id >=100;
INSERT INTO `admin_power` VALUES (100,'待办','0','','','','0','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (200,'资产','0','','','','0','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (300,'设备','0','','','','0','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (400,'库存','0','','','','0','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (500,'采购','0','','','','0','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (600,'财务','0','','','','0','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (700,'更多','0','','','','0','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (800,'软件','0','','','','700','layui-icon layui-icon-gift',80,NULL,NULL,1);

-- 待办
INSERT INTO `admin_power` VALUES (101,'待审批任务','1','','/approval/pending/main','_iframe','100','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (102,'待签字任务','1','','/approval/signature/main','_iframe','100','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (103,'待处理工单任务','1','','/approval/pendingOrder/main','_iframe','100','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (104,'待确认调拨单','1','','/approval/transfer/main','_iframe','100','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (105,'待处理员工申请','1','','/approval/applyManager/main','_iframe','100','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (106,'日程','1','','/approval/schedule/main','_iframe','100','layui-icon layui-icon-gift',60,NULL,NULL,1);

-- 资产
INSERT INTO `admin_power` VALUES (290,'首页','1','','/admin/main','_iframe','200','layui-icon layui-icon-gift',9,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (201,'资产列表','1','','/asset/asset/main','_iframe','200','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (202,'资产入库','1','','/asset/assetReceived/main','_iframe','200','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (203,'领用&退库','1','','/asset/requisitionAndRevert/main','_iframe','200','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (204,'借用&退还','1','','/asset/borrowAndReturn/main','_iframe','200','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (205,'资产调拨','1','','/asset/assetAllocation/main','_iframe','200','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (206,'实物信息变更','1','','/asset/materielChange/main','_iframe','200','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (207,'附属资产变更','1','','/asset/subAsset/main','_iframe','200','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (208,'维保信息登记','1','','/asset/maintenanceChange/main','_iframe','200','layui-icon layui-icon-gift',80,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (209,'财务信息变更','1','','/asset/financeChange/main','_iframe','200','layui-icon layui-icon-gift',90,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (210,'维修信息登记','1','','/asset/assetRepair/main','_iframe','200','layui-icon layui-icon-gift',100,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (211,'清理报废','1','','/asset/assetClear/main','_iframe','200','layui-icon layui-icon-gift',110,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (212,'盘点管理','1','','/asset/checkManagement/main','_iframe','200','layui-icon layui-icon-gift',120,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (240,'分析报表','0','','','','200','layui-icon layui-icon-gift',130,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (241,'资产清单','1','','/asset/report/assetDetailList','_iframe','240','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (242,'资产履历','1','','/asset/report/assetRecord','_iframe','240','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (243,'资产分类汇总表','1','','/asset/report/assetCategoryList','_iframe','240','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (244,'公司部门汇总表','1','','/asset/report/assetCompanyList','_iframe','240','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (245,'资产区域汇总表','1','','/asset/report/assetDistrictList','_iframe','240','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (246,'分类使用情况表','1','','/asset/report/assetCategoryUsed','_iframe','240','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (247,'月增加对账表','1','','/asset/report/monthAddReport','_iframe','240','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (248,'到期资产','1','','/asset/report/expireAsset','_iframe','240','layui-icon layui-icon-gift',80,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (249,'清理清单','1','','/asset/report/clearDetailList','_iframe','240','layui-icon layui-icon-gift',90,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (250,'折旧粗算表','1','','/asset/report/assetDepreciation','_iframe','240','layui-icon layui-icon-gift',100,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (251,'资产分类增减表','1','','/asset/report/assetCateList','_iframe','240','layui-icon layui-icon-gift',110,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (252,'维保到期统计表','1','','/asset/report/assetMaintenance','_iframe','240','layui-icon layui-icon-gift',120,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (253,'员工资产统计','1','','/asset/report/employeeAsset','_iframe','240','layui-icon layui-icon-gift',130,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (254,'标准资产型号统计','1','','/asset/report/specsReport','_iframe','240','layui-icon layui-icon-gift',140,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (255,'闲置资产共享','1','','/asset/report/assetIdleShare','_iframe','240','layui-icon layui-icon-gift',150,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (256,'呆滞资产清单','1','','/asset/report/assetOfStagnant','_iframe','240','layui-icon layui-icon-gift',160,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (270,'设置','0','','','','200','layui-icon layui-icon-gift',140,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (271,'资产分类','1','','/asset/settings/assetCategory','_iframe','270','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (272,'区域管理','1','','asset/settings/area/main','_iframe','270','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (273,'模板标签设置','1','','/asset/settings/printSetup','_iframe','270','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (274,'资产编码规则设置','1','','/asset/settings/barcodeRule','_iframe','270','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (275,'通用设置','1','','/asset/settings/generalSettings','_iframe','270','layui-icon layui-icon-gift',50,NULL,NULL,1);

-- 设备
INSERT INTO `admin_power` VALUES (301,'首页','1','','/device/dashboard/main','_iframe','300','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (302,'设备列表','1','','/device/assetAccount/main','_iframe','300','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (303,'设备入库','1','','/device/inStorage/main','_iframe','300','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (304,'附属资产变更','1','','/device/subAsset/main','_iframe','300','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (305,'配件出库','1','','/device/accessoryCheckOut/main','_iframe','300','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (306,'分析报表','0','','','','300','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (307,'设备维修','0','','','','300','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (308,'保养维护','0','','','','300','layui-icon layui-icon-gift',80,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (309,'巡检点检','0','','','','300','layui-icon layui-icon-gift',90,NULL,NULL,1);
-- 设备-分析报表
INSERT INTO `admin_power` VALUES (310,'附属资产查询','1','','/device/report/subAssetSearch','_iframe','306','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (311,'配件领用汇总','1','','/device/report/accessorySearch','_iframe','306','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (312,'设备配件查询','1','','/device/report/equipmentAccessorySearch','_iframe','306','layui-icon layui-icon-gift',30,NULL,NULL,1);
-- 设备-设备维修
INSERT INTO `admin_power` VALUES (313,'维修工单','1','','/device/repair/deviceRepair/main','_iframe','307','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (314,'维修验收','1','','/device/repair/repairedAcceptance/main','_iframe','307','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (315,'维修材料出库','1','','/device/repair/repairedOutStorage/main','_iframe','307','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (316,'分析报表','0','','','','307','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (317,'维修设置','1','','/device/repair/repairSetting/main','_iframe','307','layui-icon layui-icon-gift',50,NULL,NULL,1);
-- 设备-设备维修-分析报表
INSERT INTO `admin_power` VALUES (318,'维修信息汇总','1','','/device/repair/report/maintenanceInformationSummary','_iframe','316','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (319,'设备类型故障率','1','','/device/repair/report/equipmentTypeFailureRate','_iframe','316','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (320,'公司部门故障率','1','','/device/repair/report/failureRateOfCompanyDepartment','_iframe','316','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (321,'故障类型故障率','1','','/device/repair/report/faultTypeFailureRate','_iframe','316','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (322,'区域故障率','1','','/device/repair/report/areaFailureRate','_iframe','316','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (323,'维修成本统计','1','','/device/repair/report/maintenanceCostStatistics','_iframe','316','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (324,'维修材料统计','1','','/device/repair/report/maintenanceMaterialStatistics','_iframe','316','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (325,'维修班组统计','1','','/device/repair/report/maintenanceTeamStatistics','_iframe','316','layui-icon layui-icon-gift',80,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (326,'维修供应商统计','1','','/device/repair/report/maintainerStatistics','_iframe','316','layui-icon layui-icon-gift',90,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (327,'维修时间统计','1','','/device/repair/report/maintenanceTimeStatistics','_iframe','316','layui-icon layui-icon-gift',100,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (328,'报修人员统计','1','','/device/repair/report/applicantStatistics','_iframe','316','layui-icon layui-icon-gift',110,NULL,NULL,1);
-- 设备-保养维护
INSERT INTO `admin_power` VALUES (329,'保养方案','1','','/device/maintenance/plan/main','_iframe','308','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (330,'保养任务','1','','/device/maintenance/task/main','_iframe','308','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (331,'保养任务变更','1','','/device/maintenance/changeTask/main','_iframe','308','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (332,'保养执行','1','','/device/maintenance/performed/main','_iframe','308','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (333,'分析报表','0','','','','308','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (334,'保养设置','1','','/device/maintenance/setting/main','_iframe','308','layui-icon layui-icon-gift',60,NULL,NULL,1);
-- 设备-保养维护-分析报表
INSERT INTO `admin_power` VALUES (335,'设备保养统计','1','','/device/maintenance/report/equipmentMaintainStatistics','_iframe','333','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (336,'近期需保养设备','1','','/device/maintenance/report/recentMaintainStatistics','_iframe','333','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (337,'保养项目统计','1','','/device/maintenance/report/maintainItemStatistics','_iframe','333','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (338,'保养材料消耗统计','1','','/device/maintenance/report/maintainConsumableStatistics','_iframe','333','layui-icon layui-icon-gift',40,NULL,NULL,1);
-- 设备-巡检点检
INSERT INTO `admin_power` VALUES (339,'巡检计划','1','','/device/inspect/plan/main','_iframe','309','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (340,'巡检任务','1','','/device/inspect/task/main','_iframe','309','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (341,'整改登记','1','','/device/inspect/rectification/main','_iframe','309','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (350,'分析报表','0','','','','309','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (351,'巡检设置','1','','/device/inspect/setting/main','_iframe','309','layui-icon layui-icon-gift',50,NULL,NULL,1);
-- 设备-巡检点检-分析报表
INSERT INTO `admin_power` VALUES (352,'巡检点统计','1','','/device/inspect/report/inspecPointSta','_iframe','350','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (353,'班组巡检统计','1','','/device/inspect/report/inspecTeamSta','_iframe','350','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (354,'超时巡检统计','1','','/device/inspect/report/inspecOverTimeSta','_iframe','350','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (355,'整改统计','1','','/device/inspect/report/inspecRectificationSta','_iframe','350','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (356,'巡检点整改明细','1','','/device/inspect/report/inspecRectificationDetail','_iframe','350','layui-icon layui-icon-gift',50,NULL,NULL,1);

-- 库存
INSERT INTO `admin_power` VALUES (401,'入库单','1','','/consumables/conInStorage/main','_iframe','400','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (402,'盘盈入库单','1','','/consumables/conSurplusInStorage/main','_iframe','400','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (403,'出库单','1','','/consumables/conOutStorage/main','_iframe','400','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (404,'盘亏出库单','1','','/consumables/conLossOutStorage/main','_iframe','400','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (405,'库存调整单','1','','/consumables/storageAdjustment/main','_iframe','400','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (406,'调拨单','1','','/consumables/conTransfer/main','_iframe','400','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (407,'盘点管理','1','','/consumables/checkManagement/main','_iframe','400','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (430,'分析报表','0','','','','400','layui-icon layui-icon-gift',80,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (470,'设置','0','','','','400','layui-icon layui-icon-gift',90,NULL,NULL,1);
-- 库存-分析报表
INSERT INTO `admin_power` VALUES (431,'库存分类统计','1','','/consumables/report/conCategoryQuery','_iframe','430','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (432,'即时库存查询','1','','/consumables/report/stockQuery','_iframe','430','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (433,'收发存汇总表','1','','/consumables/report/stockSummary','_iframe','430','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (434,'收发存明细表','1','','/consumables/report/stockDetail','_iframe','430','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (435,'库存领用表','1','','/consumables/report/conUsed','_iframe','430','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (436,'库存领用明细','1','','/consumables/report/conUsedQuery','_iframe','430','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (437,'部门领用成本分析','1','','/consumables/report/collectionCostAnalysis','_iframe','430','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (438,'库存申请汇总表','1','','/consumables/report/conSummary','_iframe','430','layui-icon layui-icon-gift',80,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (439,'部门使用成本对比','1','','/consumables/report/conCostComparison','_iframe','430','layui-icon layui-icon-gift',90,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (440,'库存账龄明细','1','','/consumables/report/inventoryAgingDetails','_iframe','430','layui-icon layui-icon-gift',100,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (441,'呆滞库存统计','1','','/consumables/report/conOfStagnant','_iframe','430','layui-icon layui-icon-gift',110,NULL,NULL,1);
-- 库存-设置
INSERT INTO `admin_power` VALUES (471,'仓库设置','1','','/consumables/settings/storehouse','_iframe','470','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (472,'物品档案','1','','/consumables/settings/conCategory','_iframe','470','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (473,'通用设置','1','','/consumables/settings/signature','_iframe','470','layui-icon layui-icon-gift',30,NULL,NULL,1);

-- 采购
INSERT INTO `admin_power` VALUES (501,'采购申请','1','','/purchase/requisition/main','_iframe','500','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (502,'订单管理','1','','/purchase/order/main','_iframe','500','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (503,'订单变更','1','','/purchase/orderChange/main','_iframe','500','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (504,'收货管理','1','','/purchase/receiving/main','_iframe','500','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (505,'付款申请','1','','/purchase/payment/main','_iframe','500','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (506,'付款登记','1','','/purchase/paymentRecord/main','_iframe','500','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (520,'分析报表','0','','','','500','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (550,'设置','0','','','','500','layui-icon layui-icon-gift',80,NULL,NULL,1);
-- 采购-分析报表
INSERT INTO `admin_power` VALUES (521,'采购申请汇总','1','','/purchase/report/requisition','_iframe','520','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (522,'采购申请明细','1','','/purchase/report/requisitionDetail','_iframe','520','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (523,'采购订单汇总','1','','/purchase/report/order','_iframe','520','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (524,'采购订单明细','1','','/purchase/report/orderDetail','_iframe','520','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (525,'供应商付款汇总','1','','/purchase/report/vendorPayment','_iframe','520','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (526,'供应商供货价格分析','1','','/purchase/report/vendorPriceAnalysis','_iframe','520','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (527,'供应商价格对比','1','','/purchase/report/vendorPriceComparison','_iframe','520','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (528,'采购成本汇总分析','1','','/purchase/report/costAnalysis','_iframe','520','layui-icon layui-icon-gift',80,NULL,NULL,1);
-- 采购-设置
INSERT INTO `admin_power` VALUES (551,'资产/物品分类管理','1','','/purchase/SKU/main','_iframe','550','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (552,'通用设置','1','','/purchase/settings/main','_iframe','550','layui-icon layui-icon-gift',20,NULL,NULL,1);

-- 财务
INSERT INTO `admin_power` VALUES (601,'初始化','1','','/finance/init/main','_iframe','600','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (602,'卡片信息管理','1','','/finance/cardInfoManage/main','_iframe','600','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (603,'折旧','1','','/finance/depreciation/main','_iframe','600','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (604,'财务处置','1','','/finance/financialProperty/main','_iframe','600','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (605,'结账','1','','/finance/settlement/main','_iframe','600','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (606,'付款登记','1','','/finance/paymentRecord/main','_iframe','600','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (630,'分析报表','0','','','','600','layui-icon layui-icon-gift',70,NULL,NULL,1);
-- 财务-分析报表
INSERT INTO `admin_power` VALUES (631,'资产台账','1','','/finance/report/assetFinanceInfo','_iframe','630','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (632,'折旧明细表','1','','/finance/report/depreciationInfo','_iframe','630','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (633,'折旧汇总表','1','','/finance/report/depreciationReport','_iframe','630','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (634,'部门差异明细表','1','','/finance/report/departmentDifference','_iframe','630','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (635,'低值易耗品查询','1','','/finance/report/consumables','_iframe','630','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (636,'初始化数据','1','','/finance/report/initDataReport','_iframe','630','layui-icon layui-icon-gift',60,NULL,NULL,1);

-- 软件
INSERT INTO `admin_power` VALUES (801,'首页','1','','/software/dashboard/main','_iframe','800','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (802,'软件登记','1','','/software/register/main','_iframe','800','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (803,'软件分发','1','','/software/useRecovery/main','_iframe','800','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (804,'软件维保','1','','/software/maintenance/main','_iframe','800','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (805,'软件变更','1','','/software/change/main','_iframe','800','layui-icon layui-icon-gift',50,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (806,'软件变更','1','','/software/change/main','_iframe','800','layui-icon layui-icon-gift',60,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (830,'分析报表','0','','','','800','layui-icon layui-icon-gift',70,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (860,'设置','0','','','','800','layui-icon layui-icon-gift',80,NULL,NULL,1);
-- 软件-分析报表
INSERT INTO `admin_power` VALUES (831,'软件使用情况统计','1','','/software/report/usageStatistics','_iframe','830','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (832,'软件成本汇总','1','','/software/report/costStatistics','_iframe','830','layui-icon layui-icon-gift',20,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (833,'软件维保统计','1','','/software/report/maintainStatistics','_iframe','830','layui-icon layui-icon-gift',30,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (834,'软件使用成本分摊','1','','/software/report/useCostShareStatistics','_iframe','830','layui-icon layui-icon-gift',40,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (835,'员工软件统计','1','','/software/report/employeeSoftware','_iframe','830','layui-icon layui-icon-gift',50,NULL,NULL,1);
-- 软件-设置
INSERT INTO `admin_power` VALUES (861,'软件分类','1','','/software/settings/softwareCategory','_iframe','860','layui-icon layui-icon-gift',10,NULL,NULL,1);
INSERT INTO `admin_power` VALUES (862,'通用设置','1','','/software/settings/generalSettings','_iframe','860','layui-icon layui-icon-gift',20,NULL,NULL,1);
