"""
# 分析报表
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ReportHandler(BaseHandler):
    # 库存分类统计
    def conCategoryQuery(self):
        return self.write("main")

    # 即时库存查询
    def stockQuery(self):
        return self.write("main")

    # 收发存汇总表
    def stockSummary(self):
        return self.write("main")

    # 收发存明细表
    def stockDetail(self):
        return self.write("main")

    # 库存领用表
    def conUsed(self):
        return self.write("main")

    # 库存领用明细
    def conUsedQuery(self):
        return self.write("main")

    # 部门领用成本分析
    def collectionCostAnalysis(self):
        return self.write("main")

    # 库存申请汇总表
    def conSummary(self):
        return self.write("main")

    # 部门使用成本对比
    def conCostComparison(self):
        return self.write("main")

    # 库存账龄明细
    def inventoryAgingDetails(self):
        return self.write("main")

    # 呆滞库存统计
    def conOfStagnant(self):
        return self.write("main")
