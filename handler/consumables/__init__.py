from .ConInStorageHandler import ConInStorageHandler
from .ConSurplusInStorageHandler import ConSurplusInStorageHandler
from .ConOutStorageHandler import ConOutStorageHandler
from .ConLossOutStorageHandler import ConLossOutStorageHandler
from .StorageAdjustmentHandler import StorageAdjustmentHandler
from .ConTransferHandler import ConTransferHandler
from .CheckManagementHandler import CheckManagementHandler
from .ReportHandler import ReportHandler
from .SettingsHandler import SettingsHandler

# 库存
router = [
    (r"/consumables/conInStorage/([^/]+)", ConInStorageHandler),  # 入库单
    (r"/consumables/conSurplusInStorage/([^/]+)", ConSurplusInStorageHandler),  # 盘盈入库单
    (r"/consumables/conOutStorage/([^/]+)", ConOutStorageHandler),  # 出库单
    (r"/consumables/conLossOutStorage/([^/]+)", ConLossOutStorageHandler),  # 盘亏出库单
    (r"/consumables/storageAdjustment/([^/]+)", StorageAdjustmentHandler),  # 库存调整单
    (r"/consumables/conTransfer/([^/]+)", ConTransferHandler),  # 调拨单
    (r"/consumables/checkManagement/([^/]+)", CheckManagementHandler),  # 盘点管理
    (r"/consumables/report/([^/]+)", ReportHandler),  # 分析报表
    (r"/consumables/settings/([^/]+)", SettingsHandler),  # 设置
]