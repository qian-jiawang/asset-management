"""
# 设置
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class SettingsHandler(BaseHandler):
    # 仓库设置
    def storehouse(self):
        self.write("main")

    # 物品档案
    def conCategory(self):
        self.write("main")

    # 通用设置
    def signature(self):
        self.write("main")