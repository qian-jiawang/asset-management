"""
# Area管理
"""

from __future__ import absolute_import

import json
import traceback

import tornado
from sqlalchemy import and_
from sqlalchemy_pagination import paginate
from tornado.escape import json_decode
from tornado.escape import xhtml_escape as xss_escape

from common import session
from common.DbHelper import object_to_dict
from common.HttpHelper import authorize
from handler import BaseHandler
from models import Area


class AreaHandler(BaseHandler):
    # 页面
    @authorize("admin:area:main", log=False, verify_token=False)
    def main(self):
        return self.render_template('admin/asset/settings/area/main.html')

    # 分页数据
    @authorize("admin:area:main", log=False, verify_token=False)
    def data(self):
        # 获取请求参数
        page = xss_escape(self.get_argument('page', self.settings['config']['default_page'].encode()))
        page_size = xss_escape(self.get_argument('limit', self.settings['config']['default_page_size'].encode()))
        name = xss_escape(self.get_argument('name', ''))
        query = session.query(Area)
        rule_list = []
        if name:
            rule_list.append(Area.name.like("".join(["%", name, "%"])))
        query = query.filter(and_(*rule_list)).order_by(Area.id.desc())
        page_result = paginate(query=query, page=int(page), page_size=int(page_size))
        result = page_result.items
        data = [object_to_dict(i) for i in result]
        return self.table_api(data=data, count=page_result.total)

    # 增加页面
    @authorize("admin:area:add", log=False, verify_token=False)
    def add(self):
        return self.render_template('admin/asset/settings/area/add.html')

    # 保存数据
    @authorize("admin:area:add", log=True, verify_token=False)
    def save(self):
        req_json = json_decode(self.request.body)
        _field_dict = {}
        _field_dict["code"] = xss_escape(req_json.get('code', ''))
        _field_dict["enable"] = xss_escape(req_json.get('enable', '')) or 1
        _field_dict["name"] = xss_escape(req_json.get('name', ''))
        
        _model = Area(**_field_dict)
        try:
            # 只添加，还没有提交，如果出错还可以撤回(rollback)
            session.add(_model)
            # 提交到数据库
            session.commit()
        except Exception as e:
            session.rollback()
            print(traceback.format_exc())
            return self.fail_api()
        return self.success_api(msg="保存成功")

    # 编辑页面
    @authorize("admin:area:edit", log=False, verify_token=False)
    def edit(self):
        _id = self.get_argument('id', '')
        _model = session.query(Area).filter_by(id=_id).first()
        return self.render_template('admin/asset/settings/area/edit.html', model=object_to_dict(_model))

    # 启用
    @authorize("admin:area:edit", log=True, verify_token=False)
    def enable(self):
        req_json = json_decode(self.request.body)
        _id = req_json.get('id', '')
        if not _id:
            return self.fail_api(msg="数据错误")
        res = session.query(Area).filter_by(id=_id).update({"enable": 1})
        if res:
            return self.success_api(msg="启用成功")
        return self.fail_api(msg="出错啦")

    # 禁用
    @authorize("admin:area:edit", log=True, verify_token=False)
    def disable(self):
        req_json = json_decode(self.request.body)
        _id = req_json.get("id", "")
        if not _id:
            return self.fail_api(msg="数据错误")
        res = session.query(Area).filter_by(id=_id).update({"enable": 0})
        if res:
            return self.success_api(msg="禁用成功")
        return self.fail_api(msg="出错啦")

    @authorize("admin:area:edit", log=True, verify_token=False)
    def update(self):
        req_json = json_decode(self.request.body)
        id = req_json.get("id", "")
        data = {}
        data["code"] = xss_escape(req_json.get('code', ''))
        data["name"] = xss_escape(req_json.get('name', ''))
        
        res = session.query(Area).filter_by(id=id).update(data)
        if not res:
            return self.fail_api(msg="更新失败")
        return self.success_api(msg="更新成功")

    @authorize("admin:area:remove", log=True, verify_token=False)
    def remove(self):
        _id = self.get_argument("id", "")
        if not _id:
            return self.fail_api("数据错误!")
        res = session.query(Area).filter_by(id=_id).delete()
        if not res:
            return self.fail_api(msg="删除失败")
        return self.success_api(msg="删除成功")

    @authorize("admin:area:remove", log=True, verify_token=False)
    def batch_remove(self):
        ids = self.get_arguments('ids[]')
        if not ids:
            return self.fail_api(msg="数据错误!")
        # 返回受影响的行数
        res = session.query(Area).filter(Area.id.in_(ids)).delete()
        if not res:
            return self.fail_api(msg="删除失败")
        return self.success_api(msg="批量删除成功")