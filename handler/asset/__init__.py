from .AssetAllocationHandler import AssetAllocationHandler
from .AssetClearHandler import AssetClearHandler
from .AssetHandler import AssetHandler
from .AssetRepairHandler import AssetRepairHandler
from .BorrowAndReturnHandler import BorrowAndReturnHandler
from .CheckManagementHandler import CheckManagementHandler
from .FinanceChangeHandler import FinanceChangeHandler
from .MaintenanceChangeHandler import MaintenanceChangeHandler
from .MaterielChangeHandler import MaterielChangeHandler
from .AssetReceivedHandler import AssetReceivedHandler
from .RequisitionAndRevertHandler import RequisitionAndRevertHandler
from .SubAssetHandler import SubAssetHandler
from .ReportHandler import ReportHandler
from .SettingsHandler import SettingsHandler
from .settings.AreaHandler import AreaHandler

# 资产
router = [
    (r"/asset/asset/([^/]+)", AssetHandler),  # 资产列表
    (r"/asset/assetReceived/([^/]+)", AssetReceivedHandler),  # 资产入库
    (r"/asset/requisitionAndRevert/([^/]+)", RequisitionAndRevertHandler),  # 领用&退库
    (r"/asset/borrowAndReturn/([^/]+)", BorrowAndReturnHandler),  # 借用&退还
    (r"/asset/assetAllocation/([^/]+)", AssetAllocationHandler),  # 资产调拨
    (r"/asset/materielChange/([^/]+)", MaterielChangeHandler),  # 实物信息变更
    (r"/asset/subAsset/([^/]+)", SubAssetHandler),  # 附属资产变更
    (r"/asset/maintenanceChange/([^/]+)", MaintenanceChangeHandler),  # 维保信息登记
    (r"/asset/financeChange/([^/]+)", FinanceChangeHandler),  # 财务信息变更
    (r"/asset/assetRepair/([^/]+)", AssetRepairHandler),  # 维修信息登记
    (r"/asset/assetClear/([^/]+)", AssetClearHandler),  # 清理报废
    (r"/asset/checkManagement/([^/]+)", CheckManagementHandler),  # 盘点管理
    (r"/asset/report/([^/]+)", ReportHandler),  # 分析报表
    (r"/asset/settings/([^/]+)", SettingsHandler),  # 设置
    (r"/asset/settings/area/([^/]+)", AreaHandler),  # 设置-区域管理
]