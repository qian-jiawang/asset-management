"""
# 分析报表
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ReportHandler(BaseHandler):
    # 资产清单
    def assetDetailList(self):
        self.write("main")

    # 资产履历
    def assetRecord(self):
        self.write("main")

    # 资产分类汇总表
    def assetCategoryList(self):
        self.write("main")

    # 公司部门汇总表
    def assetCompanyList(self):
        self.write("main")

    # 资产区域汇总表
    def assetDistrictList(self):
        self.write("main")

    # 分类使用情况表
    def assetCategoryUsed(self):
        self.write("main")

    # 月增加对账表
    def monthAddReport(self):
        self.write("main")

    # 到期资产
    def expireAsset(self):
        self.write("main")

    # 清理清单
    def clearDetailList(self):
        self.write("main")

    # 折旧粗算表
    def assetDepreciation(self):
        self.write("main")

    # 资产分类增减表
    def assetCateList(self):
        self.write("main")

    # 维保到期统计表
    def assetMaintenance(self):
        self.write("main")

    # 员工资产统计
    def employeeAsset(self):
        self.write("main")

    # 标准资产型号统计
    def specsReport(self):
        self.write("main")

    # 闲置资产共享
    def assetIdleShare(self):
        self.write("main")

    # 呆滞资产清单
    def assetOfStagnant(self):
        self.write("main")