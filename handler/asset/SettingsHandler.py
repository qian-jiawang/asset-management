"""
# 设置
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class SettingsHandler(BaseHandler):
    # 资产分类
    def assetCategory(self):
        self.write("main")

    # 区域管理
    def areaManagement(self):
        self.write("main")

    # 模板标签设置
    def printSetup(self):
        self.write("main")

    # 资产编码规则设置
    def barcodeRule(self):
        self.write("main")

    # 通用设置
    def generalSettings(self):
        self.write("main")