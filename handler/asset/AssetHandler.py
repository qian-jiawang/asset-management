"""
# Asset管理
"""

from __future__ import absolute_import

import json
import traceback

import tornado
from sqlalchemy import and_
from sqlalchemy_pagination import paginate
from tornado.escape import json_decode
from tornado.escape import xhtml_escape as xss_escape

from common import session
from common.DbHelper import object_to_dict
from common.HttpHelper import authorize
from models import Asset
from handler import BaseHandler

class AssetHandler(BaseHandler):
    # 页面
    @authorize("admin:asset:main", log=False, verify_token=False)
    def main(self):
        return self.render_template('admin/asset/main.html')

    # 分页数据
    @authorize("admin:asset:main", log=False, verify_token=False)
    def data(self):
        # 获取请求参数
        page = xss_escape(self.get_argument('page', self.settings['config']['default_page'].encode()))
        page_size = xss_escape(self.get_argument('limit', self.settings['config']['default_page_size'].encode()))
        name = xss_escape(self.get_argument('name', ''))
        query = session.query(Asset)
        rule_list = []
        if name:
            rule_list.append(Asset.name.like("".join(["%", name, "%"])))
        query = query.filter(and_(*rule_list))
        page_result = paginate(query=query, page=int(page), page_size=int(page_size))
        result = page_result.items
        data = [object_to_dict(i) for i in result]
        return self.table_api(data=data, count=page_result.total)

    # 增加页面
    @authorize("admin:asset:add", log=False, verify_token=False)
    def add(self):
        return self.render_template('admin/asset/add.html')

    # 保存数据
    @authorize("admin:asset:add", log=True, verify_token=False)
    def save(self):
        req_json = json_decode(self.request.body)
        _field_dict = {}
        _field_dict["rfid"] = xss_escape(req_json.get('rfid', ''))
        _field_dict["assetTypeId"] = xss_escape(req_json.get('assetTypeId', ''))
        _field_dict["assetTypeCode"] = xss_escape(req_json.get('assetTypeCode', ''))
        _field_dict["assetTypeName"] = xss_escape(req_json.get('assetTypeName', ''))
        _field_dict["assetSpecsId"] = xss_escape(req_json.get('assetSpecsId', ''))
        _field_dict["barcode"] = xss_escape(req_json.get('barcode', ''))
        _field_dict["name"] = xss_escape(req_json.get('name', ''))
        _field_dict["specs"] = xss_escape(req_json.get('specs', ''))
        _field_dict["measureUnit"] = xss_escape(req_json.get('measureUnit', ''))
        _field_dict["sn"] = xss_escape(req_json.get('sn', ''))
        _field_dict["source"] = xss_escape(req_json.get('source', ''))
        _field_dict["purchasedDate"] = xss_escape(req_json.get('purchasedDate', ''))
        _field_dict["enterpriseId"] = xss_escape(req_json.get('enterpriseId', ''))
        _field_dict["ownCompanyId"] = xss_escape(req_json.get('ownCompanyId', ''))
        _field_dict["ownCompanyCode"] = xss_escape(req_json.get('ownCompanyCode', ''))
        _field_dict["ownCompanyName"] = xss_escape(req_json.get('ownCompanyName', ''))
        _field_dict["amount"] = xss_escape(req_json.get('amount', ''))
        _field_dict["supervisorId"] = xss_escape(req_json.get('supervisorId', ''))
        _field_dict["supervisor"] = xss_escape(req_json.get('supervisor', ''))
        _field_dict["user"] = xss_escape(req_json.get('user', ''))
        _field_dict["userEmployeeId"] = xss_escape(req_json.get('userEmployeeId', ''))
        _field_dict["userEmployeeNo"] = xss_escape(req_json.get('userEmployeeNo', ''))
        _field_dict["userEmployeeName"] = xss_escape(req_json.get('userEmployeeName', ''))
        _field_dict["useCompanyId"] = xss_escape(req_json.get('useCompanyId', ''))
        _field_dict["useCompanyCode"] = xss_escape(req_json.get('useCompanyCode', ''))
        _field_dict["useCompanyName"] = xss_escape(req_json.get('useCompanyName', ''))
        _field_dict["useDepartmentId"] = xss_escape(req_json.get('useDepartmentId', ''))
        _field_dict["useDepartmentCode"] = xss_escape(req_json.get('useDepartmentCode', ''))
        _field_dict["useDepartmentName"] = xss_escape(req_json.get('useDepartmentName', ''))
        _field_dict["serviceLife"] = xss_escape(req_json.get('serviceLife', ''))
        _field_dict["addressTypeId"] = xss_escape(req_json.get('addressTypeId', ''))
        _field_dict["addressTypeCode"] = xss_escape(req_json.get('addressTypeCode', ''))
        _field_dict["addressTypeName"] = xss_escape(req_json.get('addressTypeName', ''))
        _field_dict["address"] = xss_escape(req_json.get('address', ''))
        _field_dict["comment"] = xss_escape(req_json.get('comment', ''))
        _field_dict["picturePath"] = xss_escape(req_json.get('picturePath', ''))
        _field_dict["thumbnailPath"] = xss_escape(req_json.get('thumbnailPath', ''))
        _field_dict["vendor"] = xss_escape(req_json.get('vendor', ''))
        _field_dict["vendorName"] = xss_escape(req_json.get('vendorName', ''))
        _field_dict["vendorContact"] = xss_escape(req_json.get('vendorContact', ''))
        _field_dict["maintainer"] = xss_escape(req_json.get('maintainer', ''))
        _field_dict["maintainerName"] = xss_escape(req_json.get('maintainerName', ''))
        _field_dict["maintenanceExpiredDate"] = xss_escape(req_json.get('maintenanceExpiredDate', ''))
        _field_dict["maintenanceMemo"] = xss_escape(req_json.get('maintenanceMemo', ''))
        _field_dict["state"] = xss_escape(req_json.get('state', ''))
        _field_dict["createdBy"] = xss_escape(req_json.get('createdBy', ''))
        _field_dict["createdDate"] = xss_escape(req_json.get('createdDate', ''))
        _field_dict["deleted"] = xss_escape(req_json.get('deleted', ''))
        _field_dict["includedInBill"] = xss_escape(req_json.get('includedInBill', ''))
        _field_dict["signaturePicturePath"] = xss_escape(req_json.get('signaturePicturePath', ''))
        _field_dict["signatureStatus"] = xss_escape(req_json.get('signatureStatus', ''))
        _field_dict["lastModifyDate"] = xss_escape(req_json.get('lastModifyDate', ''))
        _field_dict["lastModifyUserId"] = xss_escape(req_json.get('lastModifyUserId', ''))
        _field_dict["dataJson"] = xss_escape(req_json.get('dataJson', ''))
        _field_dict["extensionFields"] = xss_escape(req_json.get('extensionFields', ''))
        _field_dict["tags"] = xss_escape(req_json.get('tags', ''))
        _field_dict["enable"] = xss_escape(req_json.get('enable', ''))
        _field_dict["createAt"] = xss_escape(req_json.get('createAt', ''))
        _field_dict["updateAt"] = xss_escape(req_json.get('updateAt', ''))
        
        _model = Asset(**_field_dict)
        try:
            # 只添加，还没有提交，如果出错还可以撤回(rollback)
            session.add(_model)
            # 提交到数据库
            session.commit()
        except Exception as e:
            session.rollback()
            print(traceback.format_exc())
            return self.fail_api()
        return self.success_api(msg="保存成功")

    # 编辑页面
    @authorize("admin:asset:edit", log=False, verify_token=False)
    def edit(self):
        _id = self.get_argument('id', '')
        _model = session.query(Asset).filter_by(id=_id).first()
        return self.render_template('admin/asset/edit.html', model=object_to_dict(_model))

    # 启用
    @authorize("admin:asset:edit", log=True, verify_token=False)
    def enable(self):
        req_json = json_decode(self.request.body)
        _id = req_json.get('id', '')
        if not _id:
            return self.fail_api(msg="数据错误")
        res = session.query(Asset).filter_by(id=_id).update({"enable": 1})
        if res:
            return self.success_api(msg="启用成功")
        return self.fail_api(msg="出错啦")

    # 禁用
    @authorize("admin:asset:edit", log=True, verify_token=False)
    def disable(self):
        req_json = json_decode(self.request.body)
        _id = req_json.get("id", "")
        if not _id:
            return self.fail_api(msg="数据错误")
        res = session.query(Asset).filter_by(id=_id).update({"enable": 0})
        if res:
            return self.success_api(msg="禁用成功")
        return self.fail_api(msg="出错啦")

    @authorize("admin:asset:edit", log=True, verify_token=False)
    def update(self):
        req_json = json_decode(self.request.body)
        id = req_json.get("id", "")
        data = {}
        data["rfid"] = xss_escape(req_json.get('rfid', ''))
        data["assetTypeId"] = xss_escape(req_json.get('assetTypeId', ''))
        data["assetTypeCode"] = xss_escape(req_json.get('assetTypeCode', ''))
        data["assetTypeName"] = xss_escape(req_json.get('assetTypeName', ''))
        data["assetSpecsId"] = xss_escape(req_json.get('assetSpecsId', ''))
        data["barcode"] = xss_escape(req_json.get('barcode', ''))
        data["name"] = xss_escape(req_json.get('name', ''))
        data["specs"] = xss_escape(req_json.get('specs', ''))
        data["measureUnit"] = xss_escape(req_json.get('measureUnit', ''))
        data["sn"] = xss_escape(req_json.get('sn', ''))
        data["source"] = xss_escape(req_json.get('source', ''))
        data["purchasedDate"] = xss_escape(req_json.get('purchasedDate', ''))
        data["enterpriseId"] = xss_escape(req_json.get('enterpriseId', ''))
        data["ownCompanyId"] = xss_escape(req_json.get('ownCompanyId', ''))
        data["ownCompanyCode"] = xss_escape(req_json.get('ownCompanyCode', ''))
        data["ownCompanyName"] = xss_escape(req_json.get('ownCompanyName', ''))
        data["amount"] = xss_escape(req_json.get('amount', ''))
        data["supervisorId"] = xss_escape(req_json.get('supervisorId', ''))
        data["supervisor"] = xss_escape(req_json.get('supervisor', ''))
        data["user"] = xss_escape(req_json.get('user', ''))
        data["userEmployeeId"] = xss_escape(req_json.get('userEmployeeId', ''))
        data["userEmployeeNo"] = xss_escape(req_json.get('userEmployeeNo', ''))
        data["userEmployeeName"] = xss_escape(req_json.get('userEmployeeName', ''))
        data["useCompanyId"] = xss_escape(req_json.get('useCompanyId', ''))
        data["useCompanyCode"] = xss_escape(req_json.get('useCompanyCode', ''))
        data["useCompanyName"] = xss_escape(req_json.get('useCompanyName', ''))
        data["useDepartmentId"] = xss_escape(req_json.get('useDepartmentId', ''))
        data["useDepartmentCode"] = xss_escape(req_json.get('useDepartmentCode', ''))
        data["useDepartmentName"] = xss_escape(req_json.get('useDepartmentName', ''))
        data["serviceLife"] = xss_escape(req_json.get('serviceLife', ''))
        data["addressTypeId"] = xss_escape(req_json.get('addressTypeId', ''))
        data["addressTypeCode"] = xss_escape(req_json.get('addressTypeCode', ''))
        data["addressTypeName"] = xss_escape(req_json.get('addressTypeName', ''))
        data["address"] = xss_escape(req_json.get('address', ''))
        data["comment"] = xss_escape(req_json.get('comment', ''))
        data["picturePath"] = xss_escape(req_json.get('picturePath', ''))
        data["thumbnailPath"] = xss_escape(req_json.get('thumbnailPath', ''))
        data["vendor"] = xss_escape(req_json.get('vendor', ''))
        data["vendorName"] = xss_escape(req_json.get('vendorName', ''))
        data["vendorContact"] = xss_escape(req_json.get('vendorContact', ''))
        data["maintainer"] = xss_escape(req_json.get('maintainer', ''))
        data["maintainerName"] = xss_escape(req_json.get('maintainerName', ''))
        data["maintenanceExpiredDate"] = xss_escape(req_json.get('maintenanceExpiredDate', ''))
        data["maintenanceMemo"] = xss_escape(req_json.get('maintenanceMemo', ''))
        data["state"] = xss_escape(req_json.get('state', ''))
        data["createdBy"] = xss_escape(req_json.get('createdBy', ''))
        data["createdDate"] = xss_escape(req_json.get('createdDate', ''))
        data["deleted"] = xss_escape(req_json.get('deleted', ''))
        data["includedInBill"] = xss_escape(req_json.get('includedInBill', ''))
        data["signaturePicturePath"] = xss_escape(req_json.get('signaturePicturePath', ''))
        data["signatureStatus"] = xss_escape(req_json.get('signatureStatus', ''))
        data["lastModifyDate"] = xss_escape(req_json.get('lastModifyDate', ''))
        data["lastModifyUserId"] = xss_escape(req_json.get('lastModifyUserId', ''))
        data["dataJson"] = xss_escape(req_json.get('dataJson', ''))
        data["extensionFields"] = xss_escape(req_json.get('extensionFields', ''))
        data["tags"] = xss_escape(req_json.get('tags', ''))
        data["enable"] = xss_escape(req_json.get('enable', ''))
        data["createAt"] = xss_escape(req_json.get('createAt', ''))
        data["updateAt"] = xss_escape(req_json.get('updateAt', ''))
        
        res = session.query(Asset).filter_by(id=id).update(data)
        if not res:
            return self.fail_api(msg="更新失败")
        return self.success_api(msg="更新成功")

    @authorize("admin:asset:remove", log=True, verify_token=False)
    def remove(self):
        _id = self.get_argument("id", "")
        if not _id:
            return self.fail_api("数据错误!")
        res = session.query(Asset).filter_by(id=_id).delete()
        if not res:
            return self.fail_api(msg="删除失败")
        return self.success_api(msg="删除成功")

    @authorize("admin:asset:remove", log=True, verify_token=False)
    def batch_remove(self):
        ids = self.get_arguments('ids[]')
        if not ids:
            return self.fail_api(msg="数据错误!")
        # 返回受影响的行数
        res = session.query(Asset).filter(Asset.id.in_(ids)).delete()
        if not res:
            return self.fail_api(msg="删除失败")
        return self.success_api(msg="批量删除成功")