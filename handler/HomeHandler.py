"""
# 首页
"""
from __future__ import absolute_import

import tornado

from . import BaseHandler


class HomeHandler(BaseHandler):

    # 前台首页
    def main(self):
        return self.render_template('home/index.html')

    # 产品-固定资产云
    def Features(self):
        return self.render_template('home/Features.html')

    # 产品-设备云
    def DeviceManagement(self):
        return self.render_template('home/DeviceManagement.html')

    # 产品-库存云
    def Consumables(self):
        return self.render_template('home/Consumables.html')

    # 产品-采购云
    def Procurement(self):
        return self.render_template('home/Procurement.html')

    # 产品-财务云
    def Financial(self):
        return self.render_template('home/Financial.html')

    # 产品-员工自助
    def EmployeeSelf(self):
        return self.render_template('home/EmployeeSelf.html')

    # 产品-Pass+开放平台
    def Platform(self):
        return self.render_template('home/Platform.html')

    # 产品-外设
    def Device(self):
        return self.render_template('home/Device.html')

    # 解决方案-移动APP方案
    def MobileAPP(self):
        return self.render_template('home/MobileAPP.html')

    # 解决方案-无源RFID方案
    def RFID(self):
        return self.render_template('home/RFID.html')

    # 解决方案-有源RFID解决方案
    def ActiveRFID(self):
        return self.render_template('home/ActiveRFID.html')

    # 解决方案-企业微信版固定资产云
    def Qwpage(self):
        return self.render_template('home/Qwpage.html')

    # 案例
    def Case(self):
        return self.render_template('home/Case.html')

    # 资源中心-应用下载
    def Download(self):
        return self.render_template('home/Download.html')

    # 资源中心-知识中心
    def HelpCenter(self):
        return self.render_template('home/HelpCenter.html')

    # 合作伙伴
    def Partner(self):
        return self.render_template('home/Partner.html')

    # 关于我们-企业介绍
    def CompanyInfo(self):
        return self.render_template('home/CompanyInfo.html')

    # 关于我们-发展历程
    def History(self):
        return self.render_template('home/History.html')

    # 关于我们-易点动态
    def News(self):
        return self.render_template('home/News.html')

    # 关于我们-联系我们
    def Contact(self):
        return self.render_template('home/Contact.html')
