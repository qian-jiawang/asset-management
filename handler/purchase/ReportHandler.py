"""
# 分析报表
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ReportHandler(BaseHandler):
    # 采购申请汇总
    def requisition(self):
        return self.write("采购申请汇总")

    # 采购申请明细
    def requisitionDetail(self):
        return self.write("采购申请明细")

    # 采购订单汇总
    def order(self):
        return self.write("采购订单汇总")

    # 采购订单明细
    def orderDetail(self):
        return self.write("采购订单明细")

    # 供应商付款汇总
    def vendorPayment(self):
        return self.write("供应商付款汇总")

    # 供应商供货价格分析
    def vendorPriceAnalysis(self):
        return self.write("供应商供货价格分析")

    # 供应商价格对比
    def vendorPriceComparison(self):
        return self.write("供应商价格对比")

    # 采购成本汇总分析
    def costAnalysis(self):
        return self.write("采购成本汇总分析")