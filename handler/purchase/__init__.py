from .RequisitionHandler import RequisitionHandler
from .OrderHandler import OrderHandler
from .OrderChangeHandler import OrderChangeHandler
from .ReceivingHandler import ReceivingHandler
from .PaymentHandler import PaymentHandler
from .PaymentRecordHandler import PaymentRecordHandler
from .ReportHandler import ReportHandler
from .SKUHandler import SKUHandler
from .SettingsHandler import SettingsHandler

# 采购
router = [
    (r"/purchase/requisition/([^/]+)", RequisitionHandler),  # 采购申请
    (r"/purchase/order/([^/]+)", OrderHandler),  # 订单管理
    (r"/purchase/orderChange/([^/]+)", OrderChangeHandler),  # 订单变更
    (r"/purchase/receiving/([^/]+)", ReceivingHandler),  # 收货管理
    (r"/purchase/payment/([^/]+)", PaymentHandler),  # 付款申请
    (r"/purchase/paymentRecord/([^/]+)", PaymentRecordHandler),  # 付款登记
    (r"/purchase/report/([^/]+)", ReportHandler),  # 分析报表
    # 设置 #
    (r"/purchase/SKU/([^/]+)", SKUHandler),  # 资产/物品分类管理
    (r"/purchase/settings/([^/]+)", SettingsHandler),  # 通用设置
]