"""
初始化文件
"""

from __future__ import absolute_import

from .BaseHandler import BaseHandler
from .AdminHandler import AdminHandler  # 后台主页
from .AuthHandler import AuthHandler  # 登录相关
from .HomeHandler import HomeHandler  # 首页

from handler.approval import *     # 待办
from handler.asset import *        # 资产
from handler.consumables import *  # 库存
from handler.device import *       # 设备
from handler.finance import *      # 财务
from handler.purchase import *     # 采购
from handler.software import *     # 软件
from handler.system import *       # 系统配置
