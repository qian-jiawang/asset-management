"""
# 初始化
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class InitHandler(BaseHandler):

    def main(self):
        return self.write("main")