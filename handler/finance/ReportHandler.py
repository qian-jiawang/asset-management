"""
# 分析报表
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ReportHandler(BaseHandler):

    # 资产台账
    def assetFinanceInfo(self):
        return self.write("main")

    # 折旧明细表
    def depreciationInfo(self):
        return self.write("main")

    # 折旧汇总表
    def depreciationReport(self):
        return self.write("main")

    # 部门差异明细表
    def departmentDifference(self):
        return self.write("main")

    # 低值易耗品查询
    def consumables(self):
        return self.write("main")

    # 初始化数据
    def initDataReport(self):
        return self.write("main")