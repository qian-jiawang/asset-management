from .InitHandler import InitHandler
from .CardInfoManageHandler import CardInfoManageHandler
from .FinancialChangeHandler import FinancialChangeHandler
from .DepreciationHandler import DepreciationHandler
from .FinancialPropertyHandler import FinancialPropertyHandler
from .SettlementHandler import SettlementHandler
from .PaymentRecordHandler import PaymentRecordHandler
from .ReportHandler import ReportHandler

# 财务
router = [
    (r"/finance/init/([^/]+)", InitHandler),  # 初始化
    (r"/finance/cardInfoManage/([^/]+)", CardInfoManageHandler),  # 卡片信息管理
    (r"/finance/depreciation/([^/]+)", DepreciationHandler),  # 折旧
    (r"/finance/financialProperty/([^/]+)", FinancialPropertyHandler),  # 财务处置
    (r"/finance/settlement/([^/]+)", SettlementHandler),  # 结账
    (r"/finance/paymentRecord/([^/]+)", PaymentRecordHandler),  # 付款登记
    (r"/finance/report/([^/]+)", ReportHandler),  # 分析报表
]