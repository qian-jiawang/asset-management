from .ApplyManagerHandler import ApplyManagerHandler
from .PendingHandler import PendingHandler
from .PendingOrderHandler import PendingOrderHandler
from .ScheduleHandler import ScheduleHandler
from .SignatureHandler import SignatureHandler
from .TransferHandler import TransferHandler

# 待办
router = [
    (r"/approval/pending/([^/]+)", PendingHandler),  # 待审批任务
    (r"/approval/signature/([^/]+)", SignatureHandler),  # 待签字任务
    (r"/approval/pendingOrder/([^/]+)", PendingOrderHandler),  # 待处理工单任务
    (r"/approval/transfer/([^/]+)", TransferHandler),  # 待确认调拨单
    (r"/approval/applyManager/([^/]+)", ApplyManagerHandler),  # 待处理员工申请
    (r"/approval/schedule/([^/]+)", ScheduleHandler),  # 日程
]