"""
# 日程
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ScheduleHandler(BaseHandler):

    def main(self):
        # https://gitee.com/meadhu/layui-calendar
        return self.render_template('admin/approval/schedule.html')