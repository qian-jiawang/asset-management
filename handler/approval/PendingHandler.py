"""
# 待审批任务
"""

from __future__ import absolute_import

import json
import traceback

import tornado
from sqlalchemy import and_
from sqlalchemy_pagination import paginate
from tornado.escape import json_decode
from tornado.escape import xhtml_escape as xss_escape

from common import session
from common.DbHelper import object_to_dict
from common.HttpHelper import authorize
from models import Approvetask
from handler import BaseHandler


class PendingHandler(BaseHandler):
    # 页面
    @authorize("admin:approveTask:main", log=False, verify_token=False)
    def main(self):
        return self.render_template('admin/approval/pending.html')

    # 分页数据
    @authorize("admin:approveTask:main", log=False, verify_token=False)
    def data(self):
        # 获取请求参数
        page = xss_escape(self.get_argument('page', self.settings['config']['default_page'].encode()))
        page_size = xss_escape(self.get_argument('limit', self.settings['config']['default_page_size'].encode()))
        name = xss_escape(self.get_argument('name', ''))
        query = session.query(Approvetask)
        rule_list = []
        if name:
            rule_list.append(Approvetask.name.like("".join(["%", name, "%"])))
        query = query.filter(and_(*rule_list)).order_by(Approvetask.id.desc())
        page_result = paginate(query=query, page=int(page), page_size=int(page_size))
        result = page_result.items
        # data = [object_to_dict(i) for i in result]
        data = []
        for _index, _model in enumerate(result):
            item = {
                "id": _model.id,
                "status": _model.status,
                "appInstanceCode": _model.appInstanceCode,
                "applicationTypeName": _model.applicationTypeName,
                "applyUserName": _model.applyUserInfo.realname,
                "companyName": _model.companyName,
                "departmentName": _model.departmentName,
                "createAt": _model.createAt,
            }
            data.append(item)
        return self.table_api(data=data, count=page_result.total)

    # 增加页面
    @authorize("admin:approveTask:add", log=False, verify_token=False)
    def add(self):
        return self.render_template('admin/approveTask/add.html')

    # 保存数据
    @authorize("admin:approveTask:add", log=True, verify_token=False)
    def save(self):
        req_json = json_decode(self.request.body)
        _field_dict = {}
        _field_dict["appInstanceID"] = xss_escape(req_json.get('appInstanceID', ''))
        _field_dict["appInstanceCode"] = xss_escape(req_json.get('appInstanceCode', ''))
        _field_dict["applicationTypeName"] = xss_escape(req_json.get('applicationTypeName', ''))
        _field_dict["applyUserId"] = xss_escape(req_json.get('applyUserId', ''))
        _field_dict["applyUserCode"] = xss_escape(req_json.get('applyUserCode', ''))
        _field_dict["applyUserName"] = xss_escape(req_json.get('applyUserName', ''))
        _field_dict["companyId"] = xss_escape(req_json.get('companyId', ''))
        _field_dict["companyCode"] = xss_escape(req_json.get('companyCode', ''))
        _field_dict["companyName"] = xss_escape(req_json.get('companyName', ''))
        _field_dict["departmentId"] = xss_escape(req_json.get('departmentId', ''))
        _field_dict["departmentCode"] = xss_escape(req_json.get('departmentCode', ''))
        _field_dict["departmentName"] = xss_escape(req_json.get('departmentName', ''))
        _field_dict["processInstanceId"] = xss_escape(req_json.get('processInstanceId', ''))
        _field_dict["status"] = xss_escape(req_json.get('status', ''))

        _model = Approvetask(**_field_dict)
        try:
            # 只添加，还没有提交，如果出错还可以撤回(rollback)
            session.add(_model)
            # 提交到数据库
            session.commit()
        except Exception as e:
            session.rollback()
            print(traceback.format_exc())
            return self.fail_api()
        return self.success_api(msg="保存成功")

    # 编辑页面
    @authorize("admin:approveTask:edit", log=False, verify_token=False)
    def edit(self):
        _id = self.get_argument('id', '')
        _model = session.query(Approvetask).filter_by(id=_id).first()
        return self.render_template('admin/approveTask/edit.html', model=object_to_dict(_model))

    # 启用
    @authorize("admin:approveTask:edit", log=True, verify_token=False)
    def enable(self):
        req_json = json_decode(self.request.body)
        _id = req_json.get('id', '')
        if not _id:
            return self.fail_api(msg="数据错误")
        res = session.query(Approvetask).filter_by(id=_id).update({"enable": 1})
        if res:
            return self.success_api(msg="启用成功")
        return self.fail_api(msg="出错啦")

    # 禁用
    @authorize("admin:approveTask:edit", log=True, verify_token=False)
    def disable(self):
        req_json = json_decode(self.request.body)
        _id = req_json.get("id", "")
        if not _id:
            return self.fail_api(msg="数据错误")
        res = session.query(Approvetask).filter_by(id=_id).update({"enable": 0})
        if res:
            return self.success_api(msg="禁用成功")
        return self.fail_api(msg="出错啦")

    @authorize("admin:approveTask:edit", log=True, verify_token=False)
    def update(self):
        req_json = json_decode(self.request.body)
        id = req_json.get("id", "")
        data = {}
        data["appInstanceID"] = xss_escape(req_json.get('appInstanceID', ''))
        data["appInstanceCode"] = xss_escape(req_json.get('appInstanceCode', ''))
        data["applicationTypeName"] = xss_escape(req_json.get('applicationTypeName', ''))
        data["applyUserId"] = xss_escape(req_json.get('applyUserId', ''))
        data["applyUserCode"] = xss_escape(req_json.get('applyUserCode', ''))
        data["applyUserName"] = xss_escape(req_json.get('applyUserName', ''))
        data["companyId"] = xss_escape(req_json.get('companyId', ''))
        data["companyCode"] = xss_escape(req_json.get('companyCode', ''))
        data["companyName"] = xss_escape(req_json.get('companyName', ''))
        data["departmentId"] = xss_escape(req_json.get('departmentId', ''))
        data["departmentCode"] = xss_escape(req_json.get('departmentCode', ''))
        data["departmentName"] = xss_escape(req_json.get('departmentName', ''))
        data["processInstanceId"] = xss_escape(req_json.get('processInstanceId', ''))
        data["status"] = xss_escape(req_json.get('status', ''))

        res = session.query(Approvetask).filter_by(id=id).update(data)
        if not res:
            return self.fail_api(msg="更新失败")
        return self.success_api(msg="更新成功")

    @authorize("admin:approveTask:remove", log=True, verify_token=False)
    def remove(self):
        _id = self.get_argument("id", "")
        if not _id:
            return self.fail_api("数据错误!")
        res = session.query(Approvetask).filter_by(id=_id).delete()
        if not res:
            return self.fail_api(msg="删除失败")
        return self.success_api(msg="删除成功")

    @authorize("admin:approveTask:remove", log=True, verify_token=False)
    def batch_remove(self):
        ids = self.get_arguments('ids[]')
        if not ids:
            return self.fail_api(msg="数据错误!")
        # 返回受影响的行数
        res = session.query(Approvetask).filter(Approvetask.id.in_(ids)).delete()
        if not res:
            return self.fail_api(msg="删除失败")
        return self.success_api(msg="批量删除成功")