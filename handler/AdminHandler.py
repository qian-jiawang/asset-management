"""
后台首页
"""
from __future__ import absolute_import

import copy
import json
from collections import OrderedDict

import tornado
from sqlalchemy import and_

from common import session
from common import RedisHelper
from common.DbHelper import object_to_dict
from common.HttpHelper import authorize
from config import SYSTEM_NAME
from models import Power
from . import BaseHandler


class AdminHandler(BaseHandler):

    @tornado.web.authenticated
    def main(self):
        """
        主入口 文件
        :return:
        """
        user = self.current_user if self.current_user else {"id": "", "username": "未登录"}
        return self.render_template('admin/home.html', user=user)

    @tornado.web.authenticated
    def welcome(self):
        """
        默认主页
        :return:
        """
        self.render("admin/welcome.html")

    @tornado.web.authenticated
    def rights_configs(self):
        """
        # 网站配置
        :return:
        """
        # path = "static/admin/admin/data/rights_configs.json"
        # with open(path, 'r') as load_f:
        #     # aa = load_f.read()
        #     self.jsonify(json.loads(load_f.read()))
        config = dict(
            logo={
                "title": SYSTEM_NAME,  # 网站名称
                "image": "/static/admin/admin/images/logo.png"  # 网站图标
            },
            menu={  # 菜单配置
                "data": "/admin/rights_menu",  # 菜单数据来源
                "collaspe": False,
                "accordion": True,  # 是否同时只打开一个菜单目录
                "method": "GET",
                "control": True,  # 是否开启多系统菜单模式
                "controlWidth": 700,  # 顶部菜单宽度 PX
                "select": "0",  # 默认选中的菜单项
                "async": True  # 是否开启异步菜单，false 时 data 属性设置为菜单数据，false 时为 json 文件或后端接口
            },
            tab={  # 是否开启多选项卡
                "enable": True,
                "keepState": True,  # 切换选项卡时，是否刷新页面状态
                "session": True,  # 是否开启 Tab 记忆
                "max": 30,  # 最大可打开的选项卡数量
                "index": {
                    "id": "10",  # 标识 ID , 建议与菜单项中的 ID 一致
                    "href": "/admin/welcome",  # 页面地址
                    "title": "首页"  # 标题
                }
            },
            theme={
                "defaultColor": "2",  # 默认主题色，对应 colors 配置中的 ID 标识
                "defaultMenu": "light-theme",  # 默认的菜单主题 dark-theme 黑 / light-theme 白
                "allowCustom": True  # 是否允许用户切换主题，false 时关闭自定义主题面板
            },
            colors=[{"id": "1", "color": "#2d8cf0"},
                    {"id": "2", "color": "#6a83fc"},
                    {"id": "3", "color": "#1E9FFF"},
                    {"id": "4", "color": "#FFB800"},
                    {"id": "5", "color": "darkgray"}],
            links=[],
            other={
                "keepLoad": 1200,  # 主页动画时长
                "autoHead": False  # 布局顶部主题
            },
            header=False)
        return self.jsonify(config)

    @tornado.web.authenticated
    def rights_menu(self):
        """
        右侧 菜单
        :return:
        """
        user = self.current_user
        r = RedisHelper.RedisHelper()
        conn = r.get_conn()
        redis_key = self.get_redis_key(user.get("username",), user.get("id"))
        cache = conn.get(redis_key)
        if not cache:
            menu_tree = self.make_menu_tree()
            conn.set(redis_key, json.dumps(menu_tree))
        else:
            menu_tree = json.loads(cache)
        return self.jsonify(menu_tree)


        path = "static/admin/admin/data/rights_menu.json"
        with open(path, 'r') as load_f:
            # aa = load_f.read()
            self.jsonify(json.loads(load_f.read()))


    # 生成菜单树
    def make_menu_tree(self):
        # role = current_user.role
        role = []
        powers = []
        for i in role:
            # 如果角色没有被启用就直接跳过
            if i.enable == 0:
                continue
            # 变量角色用户的权限
            for p in i.power:
                # 如果权限关闭了就直接跳过
                if p.enable == 0:
                    continue
                # 一二级菜单
                if int(p.type) == 0 or int(p.type) == 1:
                    powers.append(p)

        # result = session.query(Power).filter(Power.type.in_([0, 1])).order_by(and_(Power.id, Power.sort)).all()
        result = session.query(Power).all()
        power_dict = [object_to_dict(i) for i in result if i.enable]
        power_dict.sort(key=lambda x: x['id'], reverse=True)

        menu_dict = OrderedDict()
        for _dict in power_dict:
            _dict['title'] = _dict['name']
            _dict['href'] = _dict['url']
            _id, _parent_id = str(_dict['id']), str(_dict['parent_id'])
            if _id in menu_dict:
                # 当前节点添加子节点
                _dict['children'] = copy.deepcopy(menu_dict[_id])
                _dict['children'].sort(key=lambda item: item['sort'])
                # 删除子节点
                del menu_dict[_id]

            if _parent_id not in menu_dict:
                menu_dict[_parent_id] = [_dict]
            else:
                menu_dict[_parent_id].append(_dict)

        return sorted(menu_dict.get('0', []), key=lambda item: item['sort'])

    @staticmethod
    def get_redis_key(name: str, user_id: int) -> str:
        """redis键"""
        return f"asset_{str(user_id)}_{name}"

    @staticmethod
    def clear_cache(key: str):
        """编辑权限时调用这个函数清除一下缓存"""
        r = RedisHelper.RedisHelper()
        conn = r.get_conn()
        conn.delete(key)



