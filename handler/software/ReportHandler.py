"""
# 分析报表
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ReportHandler(BaseHandler):

    # 软件使用情况统计
    def usageStatistics(self):
        return self.write("main")

    # 软件成本汇总
    def costStatistics(self):
        return self.write("main")

    # 软件维保统计
    def maintainStatistics(self):
        return self.write("main")

    # 软件使用成本分摊
    def useCostShareStatistics(self):
        return self.write("main")

    # 员工软件统计
    def employeeSoftware(self):
        return self.write("main")