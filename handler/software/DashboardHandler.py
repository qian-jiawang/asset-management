"""
# 软件管理
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class DashboardHandler(BaseHandler):

    # 首页
    def main(self):
        return self.render("admin/software/main.html")