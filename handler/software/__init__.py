from .DashboardHandler import DashboardHandler
from .RegisterHandler import RegisterHandler
from .UseRecoveryHandler import UseRecoveryHandler
from .MaintenanceHandler import MaintenanceHandler
from .ChangeHandler import ChangeHandler
from .ReportHandler import ReportHandler
from .SettingsHandler import SettingsHandler

# 软件管理
router = [
    (r"/software/dashboard/([^/]+)", DashboardHandler),  # 首页
    (r"/software/register/([^/]+)", RegisterHandler),  # 软件登记
    (r"/software/useRecovery/([^/]+)", UseRecoveryHandler),  # 软件分发
    (r"/software/maintenance/([^/]+)", MaintenanceHandler),  # 软件维保
    (r"/software/change/([^/]+)", ChangeHandler),  # 软件变更
    (r"/software/report/([^/]+)", ReportHandler),  # 分析报表
    (r"/software/settings/([^/]+)", SettingsHandler),  # 设置
]