"""
# 设置
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class SettingsHandler(BaseHandler):

    # 软件分类
    def softwareCategory(self):
        return self.write("main")

    # 通用设置
    def generalSettings(self):
        return self.write("main")