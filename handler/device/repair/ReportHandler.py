"""
# 设备管理-设备维修-分析报表
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ReportHandler(BaseHandler):

    # 维修信息汇总
    def maintenanceInformationSummary(self):
        return self.write("main")

    # 设备类型故障率
    def equipmentTypeFailureRate(self):
        return self.write("main")

    # 公司部门故障率
    def failureRateOfCompanyDepartment(self):
        return self.write("main")

    # 故障类型故障率
    def faultTypeFailureRate(self):
        return self.write("main")

    # 区域故障率
    def areaFailureRate(self):
        return self.write("main")

    # 维修成本统计
    def maintenanceCostStatistics(self):
        return self.write("main")

    # 维修材料统计
    def maintenanceMaterialStatistics(self):
        return self.write("main")

    # 维修班组统计
    def maintenanceTeamStatistics(self):
        return self.write("main")

    # 维修供应商统计
    def maintainerStatistics(self):
        return self.write("main")

    # 维修时间统计
    def maintenanceTimeStatistics(self):
        return self.write("main")

    # 报修人员统计
    def applicantStatistics(self):
        return self.write("main")