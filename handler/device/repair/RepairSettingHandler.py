"""
# 设备管理-维修设置
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class RepairSettingHandler(BaseHandler):

    def main(self):
        return self.jsonify({'result': '设备管理'})