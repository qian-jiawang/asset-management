"""
# 设备管理-巡检点检-分析报表
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ReportHandler(BaseHandler):
    # 巡检点统计
    def inspecPointSta(self):
        return self.write("main")

    # 班组巡检统计
    def inspecTeamSta(self):
        return self.write("main")

    # 超时巡检统计
    def inspecOverTimeSta(self):
        return self.write("main")

    # 整改统计
    def inspecRectificationSta(self):
        return self.write("main")

    # 巡检点整改明细
    def inspecRectificationDetail(self):
        return self.write("main")