"""
# 设备管理
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class DashboardHandler(BaseHandler):

    def main(self):
        return self.jsonify({'result': '设备管理'})