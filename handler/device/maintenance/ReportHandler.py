"""
# 设备管理-保养维护-分析报表
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ReportHandler(BaseHandler):
    # 设备保养统计
    def equipmentMaintainStatistics(self):
        return self.write("main")

    # 近期需保养设备
    def recentMaintainStatistics(self):
        return self.write("main")

    # 保养项目统计
    def maintainItemStatistics(self):
        return self.write("main")

    # 保养材料消耗统计
    def maintainConsumableStatistics(self):
        return self.write("main")