# 设备管理
from .DashboardHandler import DashboardHandler
from .AssetAccountHandler import AssetAccountHandler
from .InStorageHandler import InStorageHandler
from .SubAssetHandler import SubAssetHandler
from .AccessoryCheckOutHandler import AccessoryCheckOutHandler
from .ReportHandler import ReportHandler
# 设备维修
from .repair.DeviceRepairHandler import DeviceRepairHandler
from .repair.RepairedAcceptanceHandler import RepairedAcceptanceHandler
from .repair.RepairedOutStorageHandler import RepairedOutStorageHandler
from .repair.ReportHandler import ReportHandler as RepairReportHandler
from .repair.RepairSettingHandler import RepairSettingHandler
# 保养维护
from .maintenance.PlanHandler import PlanHandler as MaintenancePlanHandler
from .maintenance.TaskHandler import TaskHandler as MaintenanceTaskHandler
from .maintenance.ChangeTaskHandler import ChangeTaskHandler as MaintenanceChangeTaskHandler
from .maintenance.PerformedHandler import PerformedHandler as MaintenancePerformedHandler
from .maintenance.ReportHandler import ReportHandler as MaintenanceReportHandler
from .maintenance.SettingHandler import SettingHandler as MaintenanceSettingHandler
# 巡检点检
from .siteinspect.SiteInspectPlanHandler import SiteInspectPlanHandler
from .siteinspect.SiteInspectExeHandler import SiteInspectExeHandler
from .siteinspect.RectificationHandler import RectificationHandler
from .siteinspect.ReportHandler import ReportHandler as SiteInspectReportHandler
from .siteinspect.InspectSettingHandler import InspectSettingHandler


# 设备管理
router = [
    (r"/device/dashboard/([^/]+)", DashboardHandler),  # 首页
    (r"/device/assetAccount/([^/]+)", AssetAccountHandler),  # 设备列表
    (r"/device/inStorage/([^/]+)", InStorageHandler),  # 设备入库
    (r"/device/subAsset/([^/]+)", SubAssetHandler),  # 附属资产变更
    (r"/device/accessoryCheckOut/([^/]+)", AccessoryCheckOutHandler),  # 配件出库
    (r"/device/report/([^/]+)", ReportHandler),  # 分析报表
    # 设备维修 #
    (r"/device/repair/deviceRepair/([^/]+)", DeviceRepairHandler),  # 设备维修-维修工单
    (r"/device/repair/repairedAcceptance/([^/]+)", RepairedAcceptanceHandler),  # 设备维修-维修验收
    (r"/device/repair/repairedOutStorage/([^/]+)", RepairedOutStorageHandler),  # 设备维修-维修材料出库
    (r"/device/repair/report/([^/]+)", RepairReportHandler),  # 分析报表
    (r"/device/repair/repairSetting/([^/]+)", RepairSettingHandler),  # 设备管理-维修设置
    # 保养维护 #
    (r"/device/maintenance/plan/([^/]+)", MaintenancePlanHandler),  # 保养方案
    (r"/device/maintenance/task/([^/]+)", MaintenanceTaskHandler),  # 保养任务
    (r"/device/maintenance/changeTask/([^/]+)", MaintenanceChangeTaskHandler),  # 保养任务变更
    (r"/device/maintenance/performed/([^/]+)", MaintenancePerformedHandler),  # 保养执行
    (r"/device/maintenance/report/([^/]+)", MaintenanceReportHandler),  # 分析报表
    (r"/device/maintenance/setting/([^/]+)", MaintenanceSettingHandler),  # 保养设置
    # 巡检点检 #
    (r"/device/inspect/plan/([^/]+)", SiteInspectPlanHandler),  # 巡检计划
    (r"/device/inspect/task/([^/]+)", SiteInspectExeHandler),  # 巡检任务
    (r"/device/inspect/rectification/([^/]+)", RectificationHandler),  # 整改登记
    (r"/device/inspect/report/([^/]+)", SiteInspectReportHandler),  # 分析报表
    (r"/device/inspect/setting/([^/]+)", InspectSettingHandler),  # 巡检设置
]