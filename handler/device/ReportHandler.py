"""
# 设备管理-分析报表
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class ReportHandler(BaseHandler):

    # 附属资产查询
    def subAssetSearch(self):
        return self.write("main")

    # 配件领用汇总
    def accessorySearch(self):
        return self.write("main")

    # 设备配件查询
    def equipmentAccessorySearch(self):
        return self.write("main")