from .DashboardHandler import DashboardHandler
from .DeptHandler import DeptHandler
from .DictHandler import DictHandler
from .FileHandler import FileHandler
from .LogHandler import LogHandler
from .MonitorHandler import MonitorHandler
from .PowerHandler import PowerHandler
from .RoleHandler import RoleHandler
from .SettingsHandler import SettingsHandler
from .SupplierHandler import SupplierHandler
from .TaskHandler import TaskHandler
from .UserHandler import UserHandler

router = [
    (r"/admin/system/dashboard/([^/]+)", DashboardHandler),  #
    (r"/admin/system/dict/([^/]+)", DictHandler),  # 字典管理
    (r"/admin/system/dept/([^/]+)", DeptHandler),  # 部门管理
    (r"/admin/system/file/([^/]+)", FileHandler),  # 文件管理
    (r"/admin/system/log/([^/]+)", LogHandler),  # 日志管理
    (r"/admin/system/monitor/([^/]+)", MonitorHandler),  # 监控管理
    (r"/admin/system/power/([^/]+)", PowerHandler),  # 权限管理
    (r"/admin/system/role/([^/]+)", RoleHandler),  # 角色管理
    (r"/admin/system/settings/([^/]+)", SettingsHandler),  # 配置管理
    (r"/admin/system/supplier/([^/]+)", SupplierHandler),  # 供应商管理
    (r"/admin/system/task/([^/]+)", TaskHandler),  # 任务管理
    (r"/admin/system/user/([^/]+)", UserHandler),  # 用户管理
]