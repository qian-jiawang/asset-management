"""
# 配置管理
"""
from __future__ import absolute_import

import json

from common import scheduler_proj
from common.HttpHelper import authorize
from handler import BaseHandler


class SettingsHandler(BaseHandler):
    # 任务管理
    @authorize("admin:task:remove", log=False)
    def main(self):
        return self.render_template('admin/system/settings/main.html')