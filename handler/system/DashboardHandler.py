"""
# 系统配置
"""
from __future__ import absolute_import

import tornado

from handler import BaseHandler


class DashboardHandler(BaseHandler):

    def analytics(self):
        return self.jsonify({'result': '系统配置'})