﻿var cid;
var cid2;
var apiUrl = "https://api.yideamobile.com/v1";
var accountIsExist = false;
var enpIsExist = false;
//var phoneIsExist = false;
var codeIsSended = false;
var codeIsSuccessS = false;
//前往登录
function goSignIn() {
    location.href = "/auth/login";
}


function goback() {
    codeIsSended = false;
    hideErrorMsg();
    $(".step1-box").show();
    $(".step2-box").hide();
    $(".steps").removeClass("step1 step2 step3").addClass("step1");
}

///前往注册步骤2验证
function validStep2() {
    var email = $('#email').val(),
        pwd = $('#pwd').val(),
        pwdConfirm = $('#pwdConfirm').val();
    $("#pwd,#pwdConfirm,#email,#userName").removeClass('error');
    var errorMessage = "";
    if (email.length == 0) {
        errorMessage += "邮箱账号,";
        $("#email").addClass('error');
    }
    if (pwd.length == 0) {
        errorMessage += "密码,";
        $("#pwd").addClass('error');
    }
    if (pwdConfirm.length == 0) {
        errorMessage += "确认密码,";
        $("#pwdConfirm").addClass('error');
    }
    if (errorMessage.length > 0) {
        errorMessage = errorMessage.substring(0, errorMessage.length - 1) + '不能为空!';
        showErrorMsg(errorMessage);
        return false;
    }
    if (pwd != pwdConfirm) {
        showErrorMsg('两次密码输入不一致!');
        $("#pwd").addClass('error');
        $("#pwdConfirm").addClass('error');
        return false;
    }
    if (pwd.length < 6 || pwdConfirm.length < 6) {
        showErrorMsg('密码不能小于6位!');
        $("#pwd").addClass('error');
        $("#pwdConfirm").addClass('error');
        return false;
    }
    if (email.length != 0) {
        var re = new RegExp("^[a-zA-Z0-9]+([._\\-]*[a-zA-Z0-9])*@([a-zA-Z0-9]+[-a-zA-Z0-9]*[a-zA-Z0-9]+.){1,63}[a-zA-Z0-9]+$");
        if (!re.test(email)) {
            showErrorMsg('不是合法的电子邮件!');
            $("#email").addClass('error');
            return false;
        }
    }
    if (accountIsExist) {
        $("#email").addClass('error');
        showErrorMsg("账号已存在，请重新输入！");
        return false;
    }
    return true;
}

///前往注册步骤3
function goStep3() {
    $('.step1-box').addClass('hidden');
    $('.step2-box').addClass('hidden');
    $('.step3-box').removeClass('hidden');
    $('.steps').removeClass('step2').addClass('step3');
    $('.step-2').removeClass('active').addClass('complete');
    $('.step-3').addClass('complete');
    //t2 = 3;
    //countDowns();
    //cid2 = setInterval(countDowns, 1000);
    //setTimeout(function () {
    //    location.href = "/Dashboard/Index";
    //}, 3000);
}

function verifyEmailExist() {
    //var email = $('#email').val();
    //if (email.length > 0) {
    //    var url = apiUrl + "/Enterprise/VerifyName?name=" + enp + "&callback=?";
    //    $.jsonp({
    //        "url": url,
    //        "success": function (result) {
    //            if (result.Success) {
    //                $('#errorMsg2').hide();
    //                enpIsExist = false;
    //            } else {
    //                enpIsExist = true;
    //                showErrorMsg2("该企业名称已注册，请联系您的企业管理员获取登陆账号！");
    //            }
    //        },
    //        "error": function (d, msg) {
    //            alert("出错了，请联系易点客服");
    //        }
    //    });
    //}
};

function verifyEnterpriseExist() {
    var enp = $('#enterpriseName').val();
    if (enp.length > 0) {
        var url = apiUrl + "/Enterprise/VerifyName?name=" + enp + "&callback=?";
        $.jsonp({
            "url": url,
            "success": function(result) {
                if (result.Success) {
                    $('#errorMsg2').hide();
                    enpIsExist = false;
                } else {
                    enpIsExist = true;
                    showErrorMsg2("该企业名称已注册，请联系您的企业管理员获取登陆账号！");
                }
            },
            "error": function(d, msg) {
                showErrorMsg2("出错了，请联系易点客服");
            }
        });
    }
};

//function verifyPhoneExist() {
//    var phone = $('#enterprisePhone').val();
//    if (phone.length > 0) {
//        $.ajax({
//            url: "/User/VerifyPhone?name=" + phone,
//            type: "get"
//        }).done(function (result) {
//            if (result.Success) {
//                $('#errorMsg2').hide();
//                phoneIsExist = false;
//            } else {
//                phoneIsExist = true;
//                showErrorMsg2("手机号已存在，请重新输入！");
//            }
//        });
//    }
//};

function sendValidationCode() {

    var mobilePhone = $('#enterprisePhone').val();
    $("#enterprisePhone").removeClass('error');
    var errorMessage = "";
    if (mobilePhone.length == 0) {
        $("#enterprisePhone").addClass('error');
        errorMessage += "手机号,";
    }
    if (errorMessage.length > 0) {
        showErrorMsg2("请输入您的" + errorMessage.substr(0, errorMessage.length - 1));
        return false;
    }

    var isMobile = /^(?:1\d)\d{6}(\d{3}|\*{3})$/;
    var isPhone = /^((0\d{2,3})-)?(\d{7,8})(-(\d{3,}))?$/;
    if (isMobile.test(mobilePhone) == false) {
        showErrorMsg2("请填写正确手机号！");
        return false;
    }

    codeIsSended = true;

    t = 60;
    countdown();
    cid = setInterval(countdown, 1000);
    // SMS
    eval(function(p, a, c, k, e, d) { e = function(c) { return (c < a ? "" : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!''.replace(/^/, String)) { while (c--) d[e(c)] = k[c] || e(c);
            k = [function(e) { return d[e] }];
            e = function() { return '\\w+' };
            c = 1; }; while (c--)
            if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]);
        return p; }('4 9=g.f(j.i()/k).b();4 3=2.6.8.7("m");3=2.l(3);3.5.h(3.5[0],3.5[1]);4 d=2.6.8.7(9);4 e={a:2.a.t,v:2.u.s};4 c=2.n.p(d,3,e).r;4 q=c.b(2.6.o);', 32, 32, '||CryptoJS|key|var|words|enc|parse|Utf16LE|timestamp|mode|toString|cipher|codeArray|options|floor|Math|push|now|Date|1000|MD5|SMSAUTH_B305B66C0AE5418E|TripleDES|Base64|encrypt|base64Cipher|ciphertext|Pkcs7|ECB|pad|padding'.split('|'), 0, {}))

    var objParms = { LoginAs: base64Cipher, PhoneNumber: this.phoneNumber };
    var settings = {
        "url": "https://auth.yideamobile.com/api/credential/pincode",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({ "LoginAs": base64Cipher, "PhoneNumber": mobilePhone }),
    };

    $.ajax(settings).done(function(response) {
        console.log(response);
    });

    return;
    $.ajax({
        type: "POST",
        "url": "https://auth.yideamobile.com/api/credential/pincode",
        dataType: "json",
        data: {
            "LoginAs": base64Cipher,
            "PhoneNumber": mobilePhone,
        },
        "success": function(data) {
            if (data.Success == false)
                showErrorMsg2(result.ErrorMessage);
        },
        "error": function(d, msg) {
            showErrorMsg2("出错了，请联系易点客服");
        }
    });
    // $.jsonp({
    //     "url": url,
    //     "success": function (data) {
    //         if (data.Success == false)
    //             showErrorMsg2(result.ErrorMessage);
    //     },
    //     "error": function (d, msg) {
    //         showErrorMsg2("出错了，请联系易点客服");
    //     }
    // });
};

function showErrorMsg2(text) {
    $('#errorMsg2').text(text).show();
}

function showErrorMsg(text) {
    $('#errorMsg').text(text).show();
}

function hideErrorMsg() {
    $('#errorMsg,#errorMsg2').hide();
}

function countdown() {
    if (t != 1) {
        t--;
        $('#btnSendCode').attr('disabled', 'disabled');
        $('#btnSendCode').text('重新发送(' + t + ')');
    } else {
        $('#btnSendCode').text('重新发送');
        $('#btnSendCode').removeAttr('disabled');
        window.clearInterval(cid);
    }
}

function countDowns() {
    if (t2 != 1) {
        t2--;
        $('#spanSecond').text(t2);
    } else {
        t2--;
        $('#labelSecond').empty().text('正在跳转. . .');
        window.clearInterval(cid2);
    }
}

function validStep1() {
    var eName = $('#enterpriseName').val(),
        ePhone = $('#enterprisePhone').val(),
        userName = $('#userName').val(),
        validateCode = $('#ValidationCode').val();
    $("#enterpriseName,#enterprisePhone,#userName,#ValidationCode").removeClass("error");
    var errorMessage = "";
    if (eName.length == 0) {
        errorMessage += "企业名称,";
        $("#enterpriseName").addClass('error');
    }

    if (ePhone.length == 0) {
        errorMessage += "手机号,";
        $("#enterprisePhone").addClass('error');
    }
    if (userName.length == 0) {
        errorMessage += "联系人姓名,";
        $("#userName").addClass('error');
    }
    if (validateCode.length == 0) {
        errorMessage += "验证码,";
        $("#ValidationCode").addClass('error');
    }
    if (errorMessage.length > 0) {
        errorMessage = errorMessage.substring(0, errorMessage.length - 1) + '不能为空!';
        showErrorMsg2(errorMessage);
        return false;
    }

    if (eName.length < 2) {
        showErrorMsg2("企业名称不能小于两位！");
        return false;
    }
    if (jQuery.isNumeric(eName) == true) {
        showErrorMsg2("企业名称不能全部是数字！");
        return false;
    }
    if (enpIsExist) {
        showErrorMsg2("企业名称已存在，请重新输入！");
        $("#enterpriseName").addClass('error');
        return false;
    }
    //if (phoneIsExist) {
    //    showErrorMsg2("手机号已存在，请重新输入！");
    //    $("#enterprisePhone").addClass('error');
    //    return false;
    //}
    var isMobile = /^(?:1\d)\d{6}(\d{3}|\*{3})$/;
    var isPhone = /^((0\d{2,3})-)?(\d{7,8})(-(\d{3,}))?$/;
    if (isMobile.test(ePhone) == false && isPhone.test(ePhone) == false) {
        $("#enterprisePhone").addClass('error');
        showErrorMsg2("请填写正确手机号！");
        return false;
    }
    if (!codeIsSended) {
        showErrorMsg2("请先发送验证码！");
        return false;
    }

    if (codeIsSended) {
        var validateCode = $('#ValidationCode').val();
        if (validateCode.length > 0) {
            var url = apiUrl + "/SMS/ValidationCodeForJsonp?phone=" + $('#enterprisePhone').val() + "&code=" + $('#ValidationCode').val() + "&callback=?";
            $.jsonp({
                "url": url,
                "success": function(result) {
                    codeIsSuccessS = result.Success;
                    if (codeIsSuccessS) {
                        $("#ValidationCode").removeClass('error');
                        hideErrorMsg();

                        $('.step1-box').addClass('hidden');
                        $('.step2-box').removeClass('hidden');
                        $('.steps').removeClass('step1').addClass('step2');
                        $('.step-1').removeClass('active').addClass('complete');
                        $('.step-2').addClass('active');
                        return true;
                    } else {
                        showErrorMsg2('验证码不正确！');
                        $("#ValidationCode").addClass('error');
                        return false;
                    }
                },
                "error": function(d, msg) {
                    showErrorMsg2("出错了，请联系易点客服");
                }
            });
        }
    }
}

function registerEnterprise() {
    if (validStep2()) {
        var eName = $('#enterpriseName').val(),
            ePhone = $('#enterprisePhone').val(),
            email = $('#email').val(),
            pwd = $('#pwd').val(),
            userName = $('#userName').val(),
            validateCode = $('#ValidationCode').val(),
            recommendNo = $('#enterpriseRecommendNo').val();

        var url = apiUrl + "/User/SignUpForJsonp?email=" + email + "&password=" + pwd + "&userName=" + userName + "&enterpriseName=" + eName +
            "&enterprisePhone=" + ePhone + "&enterpriseRecommendNo=" + recommendNo + "&SMSCode=" + validateCode + "&callback=?";
        $.jsonp({
            "url": url,
            "success": function(result) {
                codeIsSuccessS = result.Success;
                if (result.Success) {
                    $("#ValidationCode").removeClass('error');
                    hideErrorMsg();

                    goStep3();
                    return true;
                } else {
                    hideErrorMsg();
                    showErrorMsg(result.Message);
                    return false;
                }
            },
            "error": function(d, msg) {
                showErrorMsg("出错了，请联系易点客服");
            }
        });

    }
}