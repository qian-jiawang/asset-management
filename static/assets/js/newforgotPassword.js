﻿function changeValidateCode() {
    $('#validateCodePic').attr('src', '/User/GetValidateCode?time=' + (new Date()).getTime());
}

function sendEmail() {
    var email = $('#email').val(),
        validateCode = $('#validatecode').val();

    if (email.length == 0) {
        showErrorMsg('邮箱账号不能为空！');
        $("#email").addClass('error');
        return;
    }
    if (validateCode.length == 0) {
        showErrorMsg('验证码不能为空！');
        $("#validatecode").addClass('error');
        return;
    }
    if (email.length != 0) {
        var re = /^([a-zA-Z0-9]+[-_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[-|_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
        //if (!re.test(email)) {
	if(email.indexOf('@')==-1){
            showErrorMsg('不是合法的电子邮件!');
            $("#email").addClass('error');
            return;
        }
    }
    $.ajax({
        url: "/User/GetUserByEmail?email=" + email,
        type: "get"
    }).done(function (result) {
        if (result.Success) {
           showErrorMsg("账号不存在，请检查！")
        } else {
            $.ajax({
                url: "/User/VerifyCodeForgotPassword?code=" + $('#validatecode').val(),
                type: "post",
            }).done(function (result) {
                if (result.Success) {
                    $("#validatecode").removeClass('error');
                    $('.error-info').hide();
                    $('.signup-box').css({ 'width': '800px', 'marginLeft': '-400px' });
                    $('.signup-body').find('form').hide();
                    $('.send-successful').fadeIn();
                    $.ajax({
                        url: "/User/ForgotPassword?email=" + $('#email').val(),
                        type: "post",
                    }).done(function (result) {
                        $("#mainForm").hide();
                        $("#divSuccess").show();
                    });
                } else {
                    showErrorMsg('验证码不正确！');
                    $("#validatecode").addClass('error');
                }
            });
        }
    });
};

function showErrorMsg(text) {
    $('#errorMsg').text(text).show();
}
