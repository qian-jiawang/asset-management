﻿$('.carousel').carousel({
    interval: 5000
})
// $('.section-img-text .item').each(function(index, j){
// 	$(j).find('.message').height($(j).find('.img').height())
// })
$(window).scroll(function () {
    if ($('header').hasClass('home-header')) {
        if ($(window).scrollTop() > 80) {
            $('header').addClass("white-header").css({
                'box-shadow': '0px 0px 10px rgba(0, 0, 0, .2)'
            });
        } else {
            $('header').removeClass("white-header").css({
                'box-shadow': '0px 0px 10px rgba(0, 0, 0, 0)'
            });
        }
    } else {
        if ($(window).scrollTop() > 80) {
            $('header').css({
                'box-shadow': '0px 0px 10px rgba(0, 0, 0, .2)'
            });
        } else {
            $('header').css({
                'box-shadow': '0px 0px 10px rgba(0, 0, 0, 0)'
            });
        }
    }

})
var timer = 0;

function sortNum(a, b) {
    return b - a;
}

function accordantBox(param1, param2, param3) {
    var paramHeights = [];
    $(param1).find(param2).each(function (index, j) {
        paramHeights.push($(j).height());
    })
    paramHeights.sort(sortNum);
    $(param1).find(param3).height(paramHeights[0])

}
//<a href='/Home/PushQQ'  target='_blank'><p>售前咨询1：<br>1707324690</p><p>售前咨询2：<br>3066064417</p><p>售前咨询3：<br>3428269918</p></a>
/// 生成悬浮 联系框
var contactIcon = "<div class='contact-icon'>" +
    "<ul>" +
    "<li id='icon_contact'><div class='contact-layout'><a href='javascript:helpOnlineFront()'>售前业务</a><a href='javascript:helpOnlineService()'>售后业务</a></div><a href='javascript:' ><i class='icon icon-weixin'></i>在线咨询</a></li>" +
    "<li id='icon_tel'><div class='tel-layout'><p>400-960-1596</p></div><a href='javascript:' ><i class='icon icon-tel'></i>电话咨询</a></li>" +
    "<li id='icon_qq'><div class='qq-layout'><div><p>售前业务</p><span id='myPicture'></span></div><div><p>售后业务</p><img id='myPicture1'></div></div><a><i class='icon icon-qq'></i>微信咨询</a></li>" +
    "<li><a href='mailto:service@yideamobile.com'><i class='icon icon-email'></i>邮件咨询</a></li>" +
    "<li><a href='javascript:void(0)' class='btn-totop'><i class='icon icon-totop'></i></a></li>" +
    "</ul>" +
    "</div>";
$('.wrapper').after(contactIcon);
/// 咨询显示/隐藏 动画
$('.contact-icon #icon_contact').mouseover(function () {
    $('.contact-icon #icon_contact').addClass('active');
})
$('.contact-icon #icon_contact').mouseout(function () {
    $('.contact-icon #icon_contact').removeClass('active');
})
/// 电话显示/隐藏 动画
$('.contact-icon #icon_tel').mouseover(function () {
    $('.contact-icon #icon_tel').addClass('active');
})
$('.contact-icon #icon_tel').mouseout(function () {
    $('.contact-icon #icon_tel').removeClass('active');
})

/// QQ显示/隐藏 动画
$('.contact-icon #icon_qq').mouseover(function () {
    $('.contact-icon #icon_qq').addClass('active');
})
$('.contact-icon #icon_qq').mouseout(function () {
    $('.contact-icon #icon_qq').removeClass('active');
})

//随机显示微信码
//var images = ['/assets/images/wecwechat-colde-1.png', '/assets/images/wecwechat-colde-2.png', '/assets/images/wecwechat-colde-3.png'];
//var url = images[Math.floor(Math.random() * images.length)];
var ShowCount = 1;
var i;
var str = "";
var sImg = new Array();
var sWord = new Array();
var sLink = new Array();
sImg[0] = "/static/assets/images/wechat-code-2.png";
sImg[1] = "/static/assets/images/wechat-code-3.png";
sImg[1] = "/static/assets/images/wechat-code-5.png";


function mixArray(source) {
    var goal = [];
    for (var i = 0; i < source.length; i++) {
        var pos = Math.floor(Math.random() * (source.length - i));
        goal[i] = source[pos];
        source[pos] = source[source.length - 1 - i];
    }
    return goal;
}
var Ro = new Array();
for (var x = 0; x < sImg.length; x++) {
    Ro[x] = x;
}
Ro = mixArray(Ro);
for (var j = 0; j < ShowCount; j++) {
    str += "<img src='" + sImg[Ro[j]] + "' />";
}
document.getElementById("myPicture").innerHTML = str;

//window.onload = choosePic;
//  function choosePic() {
//    var myPix = new Array("assets/images/wechat-code-1.png", "assets/images/wechat-code-2.png");
//    var randomNum = Math.floor((Math.random() * myPix.length));
//    document.getElementById("myPicture").src = myPix[randomNum];
//}
//售后
window.onload = choosePic1;
function choosePic1() {
    var myPix1 = new Array("/static/assets/images/wechat-code-4.png", "/static/assets/images/wechat-code-1.png");
    var randomNum1 = Math.floor((Math.random() * myPix1.length));
    document.getElementById("myPicture1").src = myPix1[randomNum1];
}
//回到顶部
$('.btn-totop').click(function () {
    $('html,body').animate({
        scrollTop: '0px'
    }, 800);
});


function helpOnlineFront() {
    var left_width = $(window).width() - 800;
    var top_width = $(window).height() - 600;
    window.open("https://p.qiao.baidu.com/cps/chat?siteId=12807061&userId=19128070&siteToken=0ea73bc8ddfb18f29a94de9e6ba5030c&cp=&cr=&cw=%E5%94%AE%E5%89%8D%E5%92%A8%E8%AF%A2", "_blank", "toolbar=yes, location=no, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=yes, width=800, height=600, left=" + left_width + "px,top=" + top_width + "px");
}
function helpOnlineService() {
    var left_width = $(window).width() - 800;
    var top_width = $(window).height() - 600;
    window.open("https://p.qiao.baidu.com/cps/chat?siteId=17604192&userId=19128070&siteToken=f66d4afb940bd4575709ed8cd75850cb&cp=&cr=&cw=%E5%94%AE%E5%90%8E%E5%92%A8%E8%AF%A2", "_blank", "toolbar=yes, location=no, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=yes, width=800, height=600, left=" + left_width + "px,top=" + top_width + "px");
}
function goSignup() {
    var url = getDomainUrl();
    var para = getParams();
    window.open("//" + url + "/auth/signup" + para);
}
function goSignIn() {
    var url = getDomainUrl();
    var para = getParams();
    window.open("//" + url + "/auth/login" + para);
}
function getParams() {
    var para = "?firstPage=device";
    if (localStorage.getItem("firstPage")) {
        var val = localStorage.getItem("firstPage");
        if (val == "devicefix") {
            para = "?firstPage=devicefix";
        }
        else if (val != "device" && val != "devicefix") {
            para = "?firstPage=other";
        }
    } else {
        para = "?firstPage=other";
    }
    return para;
}
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
$(function () {
    if (getQueryString("firstPage") == "devicefix") {
        localStorage.setItem("firstPage", "devicefix");
    } else if (localStorage.getItem("firstPage")) {

    } else {
        var url = window.location.href;
        url = url.toLowerCase();
        if (url.indexOf("devicemanagement") > 0) {
            localStorage.setItem("firstPage", "device")
        } else {
            localStorage.setItem("firstPage", "other")
        }
    }
});
function getDomainUrl() {
    var url = window.location.host;
    return url;
}
//mobile
(function () {
    var o = navigator.userAgent,
        r = function (r) {
            return o.indexOf(r) > -1
        };
    if ((r("Android") && r("Mobile") || r("iPhone") || r("iPod") || r("Symbian") || r("IEMobile") || r("MI PAD"))) {
        //location.href = "/m/main";
        //alert("暂不支持手机预览, 请切换PC访问");
    }
})();
//meiqia
//(function (a, b, c, d, e, j, s) {
//    a[d] = a[d] || function () {
//        (a[d].a = a[d].a || []).push(arguments)
//    };
//    j = b.createElement(c),
//        s = b.getElementsByTagName(c)[0];
//    j.async = true;
//    j.charset = 'UTF-8';
//    j.src = 'https://static.meiqia.com/widget/loader.js';
//    s.parentNode.insertBefore(j, s);
//})(window, document, 'script', '_MEIQIA');
//_MEIQIA('entId', 'b2c1e30ce1b4599064edaf590d0bee9f');