﻿var t;
$(function () {
    localStorage.removeItem("access_token");
    $("#EmpPhone").val("");
    
    if (getCookie('fs') == "true") {
        $("#qw #btnLoginWechatEnterprise").hide();
        $("#qw .decs").hide();
        localStorage.setItem("loginType","wechat")
    }

    showLogin();
});

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
} 

///前往注册步骤1
function goSignUp() {
    location.href = "/auth/signup";
}

//登录验证不能为空
function validateEmpty() {
    var userName = $('#UserName').val(),
        pwd = $('#Password').val();
    $("#UserName,#Password").removeClass('error');
    if (userName.length == 0) {
        $("#UserName").addClass('error');
        return false;
    }
    if (pwd.length == 0) {
        $("#Password").addClass('error');
        return false;
    }
    $("#btnLogin").css("background", "#87A6FF").val("正在登录. . .");
    localStorage.setItem("loginType", "userlogin");
    $('#IsEdianedong').val(localStorage.getItem('IsEdianedong'))
}

//登录验证不能为空
function validateEmpEmpty() {
    var empPhone = $('#EmpPhone').val(),
        pwd = $('#EmpPassword').val(),
        pincode = $("#ValidationCode").val();

    $("#EmpPhone,#EmpPassword").removeClass('error');
    $("#empError").text("");
    
    if (jobNumber) {
        if (!jobNumber || !corpId) {
            $("#EmpPhone").addClass('error');
            $("#empError").text("工号或企业ID不可空");
            console.log("工号或企业ID不可空");
            return false;
        }
    } else {
        if (empPhone.length == 0) {
            $("#EmpPhone").addClass('error');
            console.log("手机号空");
            return false;
        }

        var isMobile = /^1\d{10}$/;
        var isEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        if (!isMobile.test(empPhone) && !isEmail.test(empPhone)) {
            $("#EmpPhone").addClass('error');
            $("#empError").text("请输入正确的手机号或邮箱");
            console.log("请输入正确的手机号或邮箱");
            return false;
        }
    }
    
    var isEmpPassLogin = $("#empToggleLink").data("text") == "密码登录";
    if (isEmpPassLogin) {
        if (pwd.length == 0 && !jobNumber) {
            $("#EmpPassword").addClass('error');
            console.log("密码登录空");
            return false;
        }
    } else {
        if (!jobNumber) {
            if (pincode.length == 0) {
                $("#ValidationCode").addClass('error');
                console.log("pincode空");
                return false;
            }
        }
    }

    var data;
    if (isEmpPassLogin && !jobNumber) {
        data = {
            "loginid": "EMPLOYEEWEBAUTH_943902F525774A4FAA18CBDFF0FE0058",
            "openid": pwd,
            "pincode": "",
            "phoneNumber": empPhone,
        };
    } else {
        data = {
            "loginid": "EMPLOYEEWEBAUTH_943902F525774A4FAA18CBDFF0FE0058",
            "openid": "NOPASSWORD",
            "pincode": pincode,
            "phoneNumber": empPhone,
            "jobNumber": jobNumber,
            "corpId": corpId,
        };
    }
    $.ajax({
        type: 'POST',
        url: _EmployeeLoginUrl,
        data: JSON.stringify(data),
        traditional: true,
        contentType: 'application/json',
        success: function (res) {
            if (isEmpPassLogin) {
                localStorage.setItem("loginType", "empPass");
            } else {
                localStorage.setItem("loginType", "empPincode");
            }
            if (localStorage.getItem('IsEdianedong') == 'true') {
                _EmployeeRedirectUrl = _EmployeeRedirectUrl.replaceAll('yideamobile.com', 'edianedong.com')
                console.log('替换=' + _EmployeeRedirectUrl);
            }
            location.href = _EmployeeRedirectUrl + "?token=" + res + "&jobNumber=" + jobNumber + "&corpId=" + corpId;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $("#btnEmpLogin").removeAttr("disabled").css("background", "#668eff").val("登录");
            var obj = JSON.parse(XMLHttpRequest.responseText);
            if (obj.Message) {
                $("#empError").text(obj.Message);
            } else {
                $("#empError").text(XMLHttpRequest.responseText.replace("\"", "").replace("\"", ""));
            }
        },
        beforeSend: function (xhr) {
            $("#btnEmpLogin").attr("disabled", "disabled").css("background", "#87A6FF").val("正在登录. . .");
        },
    });
}

function sendValidationCode() {
    var mobilePhone = $('#EmpPhone').val();
    $("#EmpPhone").removeClass('error');
    var errorMessage = "";
    if (mobilePhone.length == 0) {
        $("#EmpPhone").addClass('error');
        errorMessage += "手机号,";
    }
    if (errorMessage.length > 0) {
        $("#EmpPhone").addClass('error');
        return false;
    }

    var isMobile = /^(?:1\d)\d{6}(\d{3}|\*{3})$/;
    if (isMobile.test(mobilePhone) == false) {
        $("#EmpPhone").addClass('error');
        return false;
    }

    t = 60;
    countdown();
    cid = setInterval(countdown, 1000);

    $.ajax({
        url: _EmployeePincodeUrl,
        type: 'post',
        data: JSON.stringify({ PhoneNumber: mobilePhone, LoginAs: _VUE_APP_SMS_SEND_KEY }),
        contentType: 'application/json',
    }).done(function (result) {
        if (result.Success == false)
            $("#EmpPhone").addClass('error');
    });
};

function countdown() {
    if (t != 1) {
        t--;
        $('#btnSendCode').attr('disabled', 'disabled');
        $('#btnSendCode').text('重新发送(' + t + ')');
    } else {
        $('#btnSendCode').text('重新发送');
        $('#btnSendCode').removeAttr('disabled');
        window.clearInterval(cid);
    }
}

function emptoggle() {
    $("#empError").text("");
    var text = $("#empToggleLink").data("text");
    $("#empToggleLink").data("text", $("#empToggleLink").text());
    $("#empToggleLink").text(text)
    if (text != "密码登录") {
        $("#divEmpPass").show();
        $("#divEmpValidCode").hide();
        $("#EmpPhone").attr("placeholder", "请输入手机号或邮箱");
    } else {
        $("#divEmpValidCode").show();
        $("#divEmpPass").hide();
        $("#EmpPhone").attr("placeholder", "请输入手机号");
    }
}

function showLogin() {
    var loginType = localStorage.getItem("loginType");
    if (loginType == "empPass") {
        $("#liEmp").click();
        emptoggle();
    } else if (loginType == "empPincode") {
        $("#liEmp").click();
    } else if (loginType == "wechat") {
        $("#liQw").click();
    } else {
        $("#liAdmin").click();
    }
    emptoggle();
}
