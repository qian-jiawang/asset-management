import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean, Text, SmallInteger, ForeignKey
from sqlalchemy.orm import relationship, backref

from common import BaseModel


class Approvetask(BaseModel):
    """待办任务"""
    __tablename__ = 'approveTask'
    id = Column(Integer, primary_key=True, comment="主键,自增", autoincrement=True)
    appInstanceID = Column(Integer, nullable=False, comment="关联单据Id")
    appInstanceCode = Column(String(50), comment="关联单据号")
    applicationTypeName = Column(String(50), comment="单据类型")
    applyUserId = Column(Integer, ForeignKey("admin_user.id"), comment="申请人ID")
    applyUserCode = Column(String(50), comment="申请人编码")
    applyUserName = Column(String(50), comment="申请人")
    companyId = Column(Integer, comment="流程所属公司ID")
    companyCode = Column(String(50), comment="流程所属公司编码")
    companyName = Column(String(50), comment="流程所属公司")
    departmentId = Column(Integer, comment="流程所属部门ID")
    departmentCode = Column(String(50), comment="流程所属部门编码")
    departmentName = Column(String(50), comment="流程所属部门")
    processInstanceId = Column(Integer, comment="关联流程实例Id")
    status = Column(SmallInteger, comment="状态(1待处理 2已处理 3审批驳回 3待签字 4已签字 5签字驳回)")
    enable = Column(SmallInteger, default=1, comment="是否启用，取值0或1，默认1")
    createAt = Column(DateTime, default=datetime.datetime.now, comment='创建时间')
    updateAt = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')
    # 表关联
    applyUserInfo = relationship("User")

    def __repr__(self):
        return f'approveTask_{self.name}'
