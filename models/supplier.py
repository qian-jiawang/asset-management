import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean, Text, SmallInteger

from common import BaseModel


class Supplier(BaseModel):
    __tablename__ = 'supplier'
    id = Column(Integer, primary_key=True, comment="主键ID")
    name = Column(String(50), comment="供应商名称")
    contact_name = Column(String(50), comment="联系人")
    contact_phone = Column(String(50), comment="联系电话")
    bank_name = Column(String(50), comment="付款开户行")
    bank_account = Column(String(50), comment="付款开户账号")
    remark = Column(String(200), comment="备注")
    status = Column(String(20), comment="状态")
    enable = Column(SmallInteger, default=1, comment="是否启用，取值0或1，默认1")
    createAt = Column(DateTime, default=datetime.datetime.now, comment='创建时间')
    updateAt = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')