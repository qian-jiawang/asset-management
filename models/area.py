import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean, Text, SmallInteger
from sqlalchemy.orm import relationship, backref

from common import BaseModel


class Area(BaseModel):
    """地区表"""
    __tablename__ = 'area'
    id = Column(Integer, primary_key=True, comment="区域ID,主键,自增", autoincrement=True)
    code = Column(Integer, unique=True, nullable=True, comment="区域编码,非空,唯一")
    name = Column(String(50), comment="区域名称")
    enable = Column(SmallInteger, default=1, comment="是否启用，取值0或1，默认1")
    createAt = Column(DateTime, default=datetime.datetime.now, comment='创建时间')
    updateAt = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')

    def __repr__(self):
        return f'area_{self.name}'
