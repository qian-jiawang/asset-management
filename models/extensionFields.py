import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean, Text, SmallInteger
from sqlalchemy.orm import relationship, backref

from common import BaseModel


class ExtensionFields(BaseModel):
    __tablename__ = 'extensionFields'
    id = Column(Integer, primary_key=True, autoincrement=True, comment="主键, 自增")
    fieldAreaType = Column(Integer, comment="自定义属性类型，1：公有属性、2：分类属性、3：私有属性")
    name = Column(String(50), comment="自定义属性名称")
    value = Column(String(20), comment="自定义属性值")
    model = Column(String(20), comment="自定义属性标识")
    enable = Column(SmallInteger, default=1, comment="是否启用，取值0或1，默认1")
    createAt = Column(DateTime, default=datetime.datetime.now, comment='创建时间')
    updateAt = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')

