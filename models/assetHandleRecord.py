import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean, Text, SmallInteger
from sqlalchemy.orm import relationship, backref

from common import BaseModel

class AssetHandleRecord(BaseModel):
    """资产处理记录表"""
    __tablename__ = 'assetHandleRecord'
    id = Column(Integer, primary_key=True, comment="主键, 自增")
    assetId = Column(String(50), comment="资产ID")
    handleUserId = Column(String(30), comment="处理人")
    handledDate = Column(DateTime, default=datetime.datetime.now, comment="处理时间")
    handleType = Column(String(50), comment="处理类型")
    handleContent = Column(Text, comment="处理内容")
    enable = Column(SmallInteger, default=1, comment="是否启用，取值0或1，默认1")
    createAt = Column(DateTime, default=datetime.datetime.now, comment='创建时间')
    updateAt = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')