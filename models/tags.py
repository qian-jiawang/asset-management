import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean, Text, SmallInteger
from sqlalchemy.orm import relationship, backref

from common import BaseModel


class Tags(BaseModel):
    __tablename__ = 'tags'
    id = Column(Integer, primary_key=True, autoincrement=True, comment="主键, 自增")
    name = Column(String(50), comment="标签名称")
    foreColor = Column(String(50), comment="文字颜色")
    backgroundColor = Column(String(20), comment="背景颜色")
    enable = Column(SmallInteger, default=1, comment="是否启用，取值0或1，默认1")
    createAt = Column(DateTime, default=datetime.datetime.now, comment='创建时间')
    updateAt = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')


# BaseModel.metadata.create_all(tables=[BaseModel.metadata.tables["tags"], ])  # 根据模型创建数据表
# BaseModel.metadata.drop_all(tables=[BaseModel.metadata.tables["tags"], ])  # 如果字段有更新模型了是不会自动更新的，需要先删除在创建
