import datetime

from sqlalchemy import Column, Integer, String, DateTime, Text, TIMESTAMP, DECIMAL, SmallInteger
from sqlalchemy.orm import relationship, backref

from common import BaseModel


class AssetReceivedDetail(BaseModel):
    """收货管理单包含的物品详情"""
    __tablename__ = 'assetReceivedDetail'
    id = Column(Integer, primary_key=True, comment="主键, 自增")
    receivedId = Column(Integer, comment="收货管理单id")
    skuId = Column(Integer, comment="物品id（资产或耗材的原id）")
    skuName = Column(String(50), comment="物品名称")
    skuCode = Column(String(50), comment="物品编码")
    measureUnit = Column(String(50), comment="物品计量单位")
    specs = Column(String(50), comment="物品规格型号")
    skuType = Column(Integer, comment="物品类型（1:资产、2:耗材）")
    assetTypeCode = Column(String(50), comment="资产分类编码")
    assetTemplateName = Column(String(50), comment="资产标准名称")
    displayIndex = Column(Integer, comment="显示的行号（物品的序列号）")
    orderDetailId = Column(Integer, comment="关联的采购订单子表id")
    orderSerialNo = Column(Integer, comment="关联的采购订单号")
    orderQuantity = Column(Integer, comment="采购数量")
    orderAmount = Column(DECIMAL(10, 2), comment="采购金额")
    price = Column(DECIMAL(10, 2), comment="收货单价")
    amount = Column(DECIMAL(10, 2), comment="收货金额")
    taxRate = Column(float, comment="税率")
    noTaxAmount = Column(DECIMAL(10, 2), comment="不含税金额")
    warehouseCode = Column(String(50), comment="收货仓库编码")
    warehouseName = Column(String(255), comment="收货仓库名称")
    districtCode = Column(String(50), comment="收货区域编码")
    districtName = Column(String(255), comment="收货区域名称")
    arrivalQuantity = Column(Integer, comment="收货数量")
    status = Column(Integer, comment="物品状态（1:待入库、2:入库中、3:已入库、4:已退货、5:已驳回）")
    enable = Column(SmallInteger, default=1, comment="是否启用，取值0或1，默认1")
    createAt = Column(DateTime, default=datetime.datetime.now, comment='创建时间')
    updateAt = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')
