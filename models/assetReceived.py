import datetime

from sqlalchemy import Column, Integer, String, DateTime,Text,TIMESTAMP, SmallInteger
from sqlalchemy.orm import relationship, backref

from common import BaseModel


class AssetReceived(BaseModel):
    """收货管理单"""
    __tablename__ = 'assetReceived'
    id = Column(Integer, primary_key=True, comment="主键, 自增")
    serialNo = Column(String(50), unique=True, nullable=False, comment="收货管理单号")
    purchasePlatform = Column(String(50), comment="采购平台（暂时无用）")
    sourceType = Column(Integer, comment="收货单的来源类型（1:无关联采购订单、2:关联了采购订单）")
    companyId = Column(Integer, comment="收货公司id")
    companyCode = Column(String(50), comment="收货公司编码")
    companyName = Column(String(50), comment="收货公司名称")
    receivedUserId = Column(Integer, comment="收货人Id")
    receivedUserName = Column(String(30), comment="收货人名称")
    receivedTime = Column(TIMESTAMP, comment="收货时间")
    vendorId = Column(Integer, comment="供应商id")
    vendorName = Column(String(50), comment="供应商名称")
    comment = Column(Text, comment="变更单的变更说明")
    status = Column(Integer, comment="订单状态（1：草稿、2:已提交、3:已审批、4:已驳回、5:已完成）")
    details = Column(Text, comment="收货管理单包含的物品详情集合")
    attachments = Column(Text, comment="附件信息（请从pc端操作附件）")
    enable = Column(SmallInteger, default=1, comment="是否启用，取值0或1，默认1")
    createAt = Column(DateTime, default=datetime.datetime.now, comment='创建时间')
    updateAt = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now, comment='修改时间')
