import redis
from config import config_params


class RedisHelper(object):
    pool = None
    _instance = None

    @staticmethod
    def get_conn():
        if RedisHelper.pool is None:
            RedisHelper.pool = redis.ConnectionPool(host=config_params.get("REDIS_HOST"),
                                                    port=config_params.get("REDIS_PORT"),
                                                    db=config_params.get("REDIS_DB"),
                                                    password=config_params.get("PASSWORD"))
        conn = redis.Redis(connection_pool=RedisHelper.pool)
        return conn

    # 防止手欠实例化
    def __new__(cls, *args, **kw):
        if not cls._instance:
            cls._instance = super(RedisHelper, cls).__new__(cls, *args, **kw)
        return cls._instance


if __name__ == '__main__':
    r = RedisHelper()
    conn = r.get_conn()
    conn.get("abc")
